#!/usr/bin/python

import os

while True:
  tfile=raw_input("UNIFIT template file? ")
  if os.path.isfile(tfile):
    break
    
nfits=int(raw_input("Number of fits? "))

qpar=raw_input("Index variable: start, step? ")
lpar=qpar.split()
istart=float(lpar[0])
istep=float(lpar[1])

start=[]
step=[]
for ipar in range(1,10):
  qpar=raw_input("Parameter $"+str(ipar)+": start, step? ")
  if qpar=="":
    break
  lpar=qpar.split()
  start.append(float(lpar[0]))
  step.append(float(lpar[1]))
npar=ipar

for ifit in range(nfits):
  index=istart+ifit*istep
  csed="sed"
  for ipar in range(1,npar):
    par=start[ipar-1]+ifit*step[ipar-1]
    csed=csed+" -e s/\$"+str(ipar)+"/"+str(par)+"/"+" -e s/\#"+str(ipar)+"/"+str(int(par))+"/"
  csed=csed+" "+tfile+" > s3Q3nZ.uni"
  print csed
  os.system(csed)
  os.system("unifit s3Q3nZ.uni")
  os.system("printf '"+str(index)+" ' >> sequence_parf.txt")
  os.system("cat unifit_parf.txt >> sequence_parf.txt")
  os.system("cp unifit_plot.ps sequence_plot_"+str(ifit)+".ps 2> /dev/null")
  os.system("cp unifit_resplot.ps sequence_resplot_"+str(ifit)+".ps 2> /dev/null")
