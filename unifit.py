#!/usr/bin/env python3

# This is the integrated python version of UNIFIT containing
# the functions from the shell script as well as former pass 1.

# The following directories have to be changed when implementing UNIFIT:
unifit_dir="/Users/zorn/unifit"
gnuplot="/usr/local/bin/gnuplot"
# The default FORTRAN compiler has to be inserted here:
forc="/usr/local/gcc12/bin/gfortran"
opts="-w"

import sys
import os
import shutil
import filecmp
import argparse
from datetime import datetime
import re

userdir=os.getcwd()

parser=argparse.ArgumentParser(description="UNIFIT - a universal fit program")
parser.add_argument("command_file",nargs='?',help="UNIFIT command file")
parser.add_argument("-c","--recompile",action='store_true',help="force recompilation")
parser.add_argument("-r","--rewrite_history",action='store_true',help="delete and restart history")
parser.add_argument("-x","--extend",action='append',type=str,help="extend command file by a command")
parser.add_argument("-v","--version",type=str,help="run UNIFIT version from another directory")

args=parser.parse_args()
cfilen=args.command_file
if not cfilen:
  cfilen="unifit.input"
cfilep=userdir+"/"+cfilen
recompile=args.recompile
xclist=args.extend
vdir=args.version
if vdir:
  unifit_dir=vdir

if args.rewrite_history:
  qdel=input("Really delete the history file? ")
  if (qdel[0].lower()=="y"):
    try:
      os.remove(userdir+"/unifit.history")
    except:
      pass

os.chdir(unifit_dir)

uinp=open("unifit_p2_inp",'w')
uinp.write(userdir+"\n")
if os.path.isfile(cfilep):
  uinp.write(cfilen+"\n")
else:
  print("Command file %s not found." % cfile)
  sys.exit(300)

# ==============================================================
# Here follows the code which was formerly in unifit_p1 (pass1):

parnam=[]
sfile=[]
code=""
defdict={}
subini=None
ffile=None

cfile=open(cfilep)
for line in cfile:

  # Everything converted to lowercase as before. (Think about it!)
  cargs=line.lower().split()
  if len(cargs)>0:
    stmt=cargs[0]
  else:
    continue
  # print(stmt)

# Blocks which are irrelevant for pass 2 are handled first:

  if (stmt[0:4]=="comm"):
    for coline in cfile:
      if (coline[0:3].lower()=="end"):
        break
    continue

  if (stmt[0:3]=="def"):
    defname=line[:-1].split()[1]
    defl=[]
    for dline in cfile:
      if (dline[0:3].lower()=="end"):
        break
      defl.append(dline)
    defdict[defname]=defl
    # print(defdict)
    continue

# The 'inline' block is still necessary for including in unifit_para.txt
  if (stmt[0:6]=="inline"):
    uinp.write(line)
    for cline in cfile:
      uinp.write(cline)
      if (cline[0:3].lower()=="end"):
        uinp.write(cline)
        break
      code=code+cline
    # print(code)
    continue

  if (stmt[0:3]=="run"):
    rfile=open("unifit_run",'w')
    for rline in cfile:
      if (rline[0:3].lower()=="end"):
        break
      rfile.write(rline)
    rfile.close()
    continue

# definitions are expanded into unifit_p2_inp:
  try:
    definition=defdict[stmt]
  except:
    pass
  else:
    for defline in definition:
      # expline=""
      # for defarg in defline:
      #   if (defarg[0]=="$"):
      #     try:
      #       pnum=int(defarg[1:])
      #     except:
      #       expline=expline+defarg+" "
      #     else:
      #       if (pnum<len(defline)):
      #         expline=expline+cargs[pnum]+" "
      #       else:
      #         expline=expline+" "
        # else:
        #   expline=expline+defarg+" "
      expline=defline
      for iarg in range(len(cargs)-1,0,-1):
        placeholder=r"\$"+str(iarg)
        expline=re.sub(placeholder,cargs[iarg],expline)
      uinp.write(expline)
    continue

# All other commands are copied into unifit_p2_inp
  uinp.write(line)

  if (stmt[0:3]=="par"):
    parnam.append(cargs[2])
    # print(cargs[2])
    continue

  if (stmt[0:3]=="for"):
    ffile=line[:-1].split(maxsplit=1)[1]
    # print(ffile)
    continue

  if (stmt[0:4]=="comp"):
    forc=line[:-1].split(maxsplit=1)[1]
    # print(forc)
    continue

  if (stmt[0:3]=="opt"):
    opts=opts+" "+line[:-1].split(maxsplit=1)[1]
    # print(opts)
    continue

  if (stmt[0:3]=="sub"):
    sfile.append(line[:-1].split(maxsplit=1)[1])
    # print(sfile)
    continue

  if (stmt[0:4]=='init'):
    subini=line[:-1].split(maxsplit=1)[1]
    # print(subini)
    continue

# append additional commands from -x switches:
if xclist:
  for xcomm in xclist:
    uinp.write(xcomm+"\n")

# close expanded actual unifit input:
uinp.close()

# generate FORTRAN compilable module:

funmod=open("funmod.f",'w')

# universal header:
funmod.write("""      real*8 function fitfun(x)

      implicit real*8 (a-h,o-z)
      dimension xdel(20),wdel(20)
      real*8 x
""")

# parameter declarations and mapping to common:
for par in parnam:
  funmod.write("     &       ,"+par+"\n")
funmod.write("      common/fitpar/ npar,idf,newpar,idel,xdel,wdel,aconv,bconv\n")
for par in parnam:
  funmod.write("     &               ,"+par+"\n")
funmod.write("!$OMP THREADPRIVATE(/fitpar/)\n\n")

# optional external FORTRAN file:
if ffile:
  try:
    exf=open(userdir+"/"+ffile,'r')
  except:
    print("Fortran file "+ffile+" does not exist")
    sys.exit(301)
  for fline in exf:
    funmod.write(fline)
  exf.close()

# (mandatory) inline FORTRAN code:
funmod.write(code)
funmod.write("""
      fitfun=y
      return
      end

""")

# optional initialisation subroutine:
funmod.write("      subroutine fn_ini\n")
if subini:
  funmod.write("      call "+subini+"\n")
funmod.write("""      return
      end


""")

# optional external subroutine files:
for sfname in sfile:
  try:
    exsf=open(userdir+"/"+sfname,'r')
  except:
    print("Subroutine file "+sfname+" does not exist")
    sys.exit(302)
  for fline in exsf:
    funmod.write(fline)
  exsf.close()

funmod.close()

# generate FORTRAN compilation command:

mfile=open("make_p2",'w')
mfile.write(forc+" "+opts+" unifit_p2.f hslsub.f kova.f funmod.f -o unifit_p2 -I '"+userdir+"'\n")
mfile.close()

# ==============================================================

# exit here for test
# print("EXIT - TEST VERSION")
# sys.exit(0)

try:
  changed=not(filecmp.cmp("funmod.f","funmod.f.old") and (filecmp.cmp("make_p2","make_p2.old")))
except:
  recompile=True
else:
  recompile=recompile or changed
recompile=recompile or not(os.path.isfile("unifit_p2"))
if recompile:
  shutil.copy2("funmod.f","funmod.f.old")
  shutil.copy2("make_p2","make_p2.old")
  print("*** Recompiling fit function...")
  os.chmod("make_p2",0o744)
  try:
    os.remove("unifit_p2")
  except:
    pass
  cexit=os.system(unifit_dir+"/make_p2")
  if (cexit!=0):
    print("Compilation failed, exit status: %i" % cexit)
    sys.exit(cexit)

if os.path.isfile(cfilep):
  print("*** Starting fit...")
  os.chdir(userdir)
  if os.path.isfile(unifit_dir+"/unifit_run"):
    os.chmod(unifit_dir+"/unifit_run",0o744)
    os.system(unifit_dir+"/unifit_run")
    os.remove(unifit_dir+"/unifit_run")
  rexit=int(os.system(unifit_dir+"/unifit_p2 < "+unifit_dir+"/unifit_p2_inp")/256)
  # print("Pass 2 exit code: %i"%rexit)
  if (rexit<100):
    hist=open("unifit.history",'a+')
    hist.write("========================================================================\n")
    now=datetime.now()
    dt_string=now.strftime("%d.%m.%Y %H:%M:%S")
    hist.write("%s, Fit executed with control file %s:\n" % (dt_string,cfilen))
    cfile.seek(0,0)
    hist.write(cfile.read())
    hist.write("==> Fit results\n")
    pfile=open("unifit_para.txt")
    hist.write(pfile.read())
    hist.close()
  if ((rexit&2)==0):
    print("*** Starting plot...")
    os.system(gnuplot+" -persist unifit_gnuplot.input")
  if ((rexit&1)==0):
    print("*** Starting residuals plot...")
    os.system(gnuplot+" -persist unifit_resplot.input")
  if (rexit==7):
    print("Function calculation not requested -> no plot.")
  elif (rexit==3):
    print("No plots requested.")
  elif (rexit>=8):
    print("Fit run-time failed, exit status: %i" % rexit)
