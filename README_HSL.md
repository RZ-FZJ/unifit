# HSL Subroutines

UNIFIT requires a couple of routines from the Harwell Software Library (HSL).
Because the HSL routines are
licensed for academic use but not for redistribution they cannot be included in
this package but have to be downloaded by the user from

(1) http://www.hsl.rl.ac.uk/catalogue/

and

(2) http://www.hsl.rl.ac.uk/archive/index.html

The following routines will be required:

* VA05 (2) (including MB11)
* MA10 (2)
* KB07 (1)
* QA05 (2)
* FD05 (2)

MB11 is included in one package (ddeps) with VA05.

Note that in all cases the double precision variants (.....D) are used. All
routines have to be placed in a single file hslsub.f. This file, together with
the file kova.f (which is included in this package) will be used as additional
input files when a recompilation of the pass 2 code is triggered.

Some arrays are defined to length one in VA05AD. This may cause errors for
certain FORTRAN compilers. (What matters here is the compiler chosen in the
compiler statement in the UNIFIT control file or the default in unifit_par.f -
not the compiler used to compile unifit_p1.) If this happens you either have to
disable array bounds checking or add the following statement to VA05AD:

`      include "unifit_par.f"`

and replace

`      DIMENSION F(1),X(1),W(1)`

by

`      DIMENSION F(mdatt),X(mptf),W(mw)`
