      SUBROUTINE KOVA(X,Y,W,PAR,dpar,H,M,N,NR)

C     Warning: The following original comments seem to be
C     partially inappropriate for the current version of KOVA!

C
C
C     DIESE SUBROUTINE BERECHNET DIE STANDARDABWEICHUNG DER MIT
C     S.R. VA05AD GEFITTETEN PARAMETER UND DIE KORRELATIONSMATRIX
C
C     DIESE S.R. KANN NUR IM ZUSAMMENHANG MIT S.R. VA05AD BENUTZT
C     WERDEN DURCH FOLGENDE AUFRUFE:
C
C     CALL VA05AD(M,N,F,PAR,H,DMAX,ACC,MAXFUN,IPRINT,WO)
C
C     CALL KOVA(X,Y,W,PAR,HP,M,N,IW)
C
C     ZUSAETZLICH MUESSEN FOLGENDE COMMON-BLOECKE VEREINBART WERDEN:
C     1) IM RUFENDEN PROGRAMM
C        COMMON /CAL/ X(150),Y(150)
C        COMMON /GEW/ DSW(150)
C
C     2) IN S.R. CALFUN
C        COMMON /FF/ ALOR(150)
C        COMMON /CAL/ X(150),Y(150)
C        COMMON /GEW/ DSW(150)
C
C     AUSSERDEM MUESSEN IN S.R. CALFUN DIE F(I) MIT DSW(I) MULTIPLIZIER
C     WERDEN, WOBEI DSW(I)=DSQRT(W(I)) IST.
C     DSW MUSS VOR DEM AUFRUF VON S.R. VA05AD DEFINIERT SEIN
C
C     PARAMETERBESCHREIBUNG
C     (WEITERE PARAMETERBESCHR. SIEHE VA05AD)
C
C     X(I),I=1,M        ABSZISSE DER MESSWERTE
C     Y(I),I=1,M        ORDINATE DER MESSWERTE
C     W(I),I=1,M        GEWICHTE
C     PAR(I),I=1,N      PARAMETER (VOR AUFRUF VON VA05AD: STARTWERTE)
C     PGUES(I),I=1,N    STARTWERTE DER PARAMETER
C     M                 ANZAHL DER MESSWERTE
C     N                 ANZAHL DER PARAMETER
C     HP                DELTA(PAR(L)) (ZUR BERECHNUNG DER NUMERISCHEN A
C                       Z. B. HP = 10.**(-6)
C     DSW(I),I=1,M      DSQRT(W(I))
C     ALOR(I),I=1,M     FUNKTIONSWERTE DER THEORIEFKT IN S.R. CALFUN
C
C     ALLE REAL-VARIABLEN MUESSEN MIT REAL*8 DEKLARIERT SEIN
C     DER TYP DER VARIABLEN (INTEGER ODER REAL) ENTSPRICHT
C     DER STANDARD-FORTRAN DEKLARARTION
C
C     ES WERDEN NOCH FOLGENDE UNTERPROGRAMME AUS DER HSL BENOETIGT:
C     1) MA10AD
C     2) MC03AD (?)
C
C     EINSCHRAENKUNGEN:
C     M <= 2000 (mdat) UND N <= 400

      include "unifit_par.f"

C     ERFUELLEN M UND N NICHT DIESE BEDINGUNGEN,
C     MUESSEN DIE DIMENSIONSANGABEN IN DEN PROGRAMMEN
C     DIE ERSTE ZUWEISUNG IN S.R. KOVA ( IA = 400 )
C     UND DIE ERSTE ABFRAGE IN S.R. KOVA ENTSPRECHEND
C     GEAENDERT WERDEN
C
C
C
C     .. Scalar Arguments ..
      REAL*8 H
      INTEGER IW,M,N
C     ..
C     .. Array Arguments ..
      REAL*8 PAR(N),W(M),X(M),Y(M),dpar(n)
C     ..
C     .. Arrays in Common ..
      REAL*8 FCT(mdat)
C     ..
C     .. Local Scalars ..
      REAL*8 DIF,DP,FI2,SUM,SUMF,SUMG
      INTEGER I,IA,IB,J,K,L,NR
C     ..
C     .. Local Arrays ..
      REAL*8 AM(400,400),B(1,1),DF(mdat,400),F(mdat),FPL(mdat)
      real*8 PPL(mdat),RHO(400,400)
C     ..
C     .. External Functions ..
      REAL*8 DFLOAT
C     EXTERNAL DFLOAT
C     ..
C     .. External Subroutines ..
      EXTERNAL CALFUN,MA10AD
C     ..
C     .. Intrinsic Functions ..
      INTRINSIC SQRT
C     ..
C     .. Common blocks ..
      COMMON /FF/FCT
C     COMMON /PG/PGUES,PAR1
C     ..

      IF (M.GT.mdat .OR. N.GT.400 .OR. M.LT.0 .OR. N.LT.0) THEN
         WRITE (20,FMT=9090) M,N

      ELSE
         IA = 400
         IB = 1

         CALL CALFUN(M,N,F,PAR)
         SUMF = 0.D0
         SUMG = 0.D0
         nbad=0
         DO 20 I = 1,M
            FI2 = F(I)*F(I)
            SUMF = SUMF + FI2
            if (w(i).gt.0.0) then
              SUMG = SUMG + FI2/W(I)
            else
              nbad=nbad+1
            end if
 20      CONTINUE

C     The following line was included to effectively rescale the errors
C     to values yielding chi^2=m-n which is the expected value if the
C     theory is 'correct'. If the errors are trustworthy but the theory
C     known to be approximative it should be omitted. If the rescaling
C     is done it is better to excluded 'bad' points, because they are
C     usually marked as bad because the theory is inappropriate for them.

         SUMF = SUMF/DFLOAT(M-N-nbad)

         DO 30 I = 1,M
            FPL(I) = FCT(I)
   30    CONTINUE
         DO 40 I = 1,N
            PPL(I) = PAR(I)
   40    CONTINUE
         DO 50 L = 1,N
            DP = H*PPL(L)
            IF (DP.EQ.0.D0) DP = H
            PAR(L) = PPL(L) + DP
            CALL CALFUN(M,N,F,PAR)
            PAR(L) = PPL(L)
            DO 60 I = 1,M
               DF(I,L) = (FCT(I)-FPL(I))/DP
   60       CONTINUE
   50    CONTINUE

         write(20,*) 'Derivative Matrix:'
         write(20,'(2147483647i10)') (k,k=1,n)
         do i=1,m
           write(20,'(i4,400e10.2)') i,(df(i,k),k=1,n)
         end do

         DO 70 K = 1,N
            DO 80 J = 1,K
               SUM = 0.D0
               DO 90 I = 1,M
                  SUM = SUM + W(I)*DF(I,K)*DF(I,J)
   90          CONTINUE
               AM(K,J) = SUM
               AM(J,K) = SUM
   80       CONTINUE
   70    CONTINUE

         write(20,*)
         write(20,*) 'Curvature Matrix:'
         write(20,'(3x,2147483647i10)') (k,k=1,n)
         do i=1,n
           write(20,'(i4,400e10.2)') i,(am(i,k),k=1,n)
         end do

C
C     INVERSION DER MATRIX AM
C
         CALL MA10AD(AM,B,N,0,NR,1,IA,IB)
C
C
         IF (NR.EQ.0) THEN
            DO 100 I = 1,N
               DO 110 K = 1,I
                  RHO(I,K) = AM(I,K)/ (SQRT(AM(I,I))*SQRT(AM(K,K)))
                  RHO(K,I) = RHO(I,K)
  110          CONTINUE
  100       CONTINUE
C
C
C     OUTPUT (AEHNL. S.R. LEAST)
C
            WRITE (20,FMT=9000) M,N
            WRITE (20,FMT=9010) SUMF,SUMG
            WRITE (20,FMT=9030)
            DO 120 K = 1,N
               SUM = 0.D0
               DO 130 I = 1,M
                  SUM = SUM + W(I)* (Y(I)-FPL(I))*DF(I,K)
  130          CONTINUE
               SUM = -2.D0*SUM
               DIF = SQRT(AM(K,K)*SUMF)
               dpar(K) = DIF
C     The guess is always 0.0 for after normalisation by UNIFIT:
               WRITE (20,FMT=9040) K,0.0,PAR(K),DIF,SUM
  120       CONTINUE
            WRITE (20,FMT=9050)
            DO 140 I = 1,N
               WRITE (20,FMT=9060) I, (RHO(I,K),K=1,N)
  140       CONTINUE
            WRITE (20,FMT=9070)
            DO 150 I = 1,M
               DIF = Y(I) - FPL(I)
               WRITE (20,FMT=9080) I,W(I),X(I),Y(I),FPL(I),DIF
  150       CONTINUE

         ELSE
            WRITE (20,FMT=9020) NR
C           call exit(110)

         END IF

      END IF
C
C
C     F O R M A T E
C
 9000 FORMAT ('1',/,/,/,1X,'S.R. KOVA',/,/,' THIS PROBLEM CONTAINS',I4,
     *       ' DATA POINTS AND',I3,' PARAMETERS.')
 9010 FORMAT (1X,'THE WEIGHTED VARIANCE IS',D14.7,' (CHI-SQUARE:',
     *       ' SS/(M-N))',/,' AND THE UNWEIGHTED',
     *       ' SUM OF SQUARES OF THE DEVIATIONS IS',D14.7,'.')
 9020 FORMAT (1X,'***** FEHLER: S.R. MA10AD (NR =',I5,')')
 9030 FORMAT (/,/,/,6X,'GUESSTIMATE OF   FINAL VALUE OF      S.D.  OF',
     *       /,2X,'K',3 (3X,'K-TH PARAMETER'),6X,'GRADIENT',/)
 9040 FORMAT (I3,4D17.7)
 9050 FORMAT (/,/,1X,'MATRIX OF CORRELATIONS BETWEEN FREE PARAMETERS',/)
 9060 FORMAT (3X,I2,2147483647F8.3)
 9070 FORMAT (/,/,/,28X,'INDEPENDENT',6X,'DEPENDENT',7X,'CALCULATED',/,
     *       5X,'I',6X,'WEIGHT',1X,2 (9X,'VARIABLE'),9X,'FUNCTION',8X,
     *       'DEVIATION',/)
 9080 FORMAT (1X,I4,5D17.7)
 9090 FORMAT (1X,'S.R. KOVA: FEHLER( M ODER N UNZULAESSIG)',/,1X,'M = ',
     *       I10,3X,'N =',I10)

      END
