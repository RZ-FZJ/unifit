#!/usr/bin/env python3

# This is the new python-based pass 1

# Using python avoids the awkward string handling by FORTRAN. It allows
# directory and file names including spaces. Also it should be easier to
# implement changes.

# There is no check for dimensions. This should be done based on the values
# in unifit_par.f. (Anyway, these checks were not done in the FORTRAN version too.)

import sys

aui1=open("act_uni_inp1",'w')

udir=sys.stdin.readline()[:-1]
aui1.write(udir+"\n")
cfname=sys.stdin.readline()[:-1]
aui1.write(cfname+"\n")

# print(udir)
# print(cfname)
# print("=================")

parnam=[]
forc="gfortran"
opts=""
sfile=[]
code=""
defdict={}
subini=None
ffile=None

for line in sys.stdin:

  # print(line[:-1]
  # Everything converted to lowercase as before. (Think about it!)
  cargs=line.lower().split()
  if len(cargs)>0:
    stmt=cargs[0]
  else:
    continue
  # print(stmt)

# Blocks which are irrelevant for pass 2 are handled first:

  if (stmt[0:4]=="comm"):
    for coline in sys.stdin:
      if (coline[0:3].lower()=="end"):
        break
    continue

  if (stmt[0:3]=="def"):
    defname=line[:-1].split()[1]
    defl=[]
    for dline in sys.stdin:
      if (dline[0:3].lower()=="end"):
        break
      defl.append(dline.split())
    defdict[defname]=defl
    print(defdict)
    continue

  if (stmt[0:6]=="inline"):
    for cline in sys.stdin:
      if (cline[0:3].lower()=="end"):
        break
      code=code+cline
    # print(code)
    continue

  if (stmt[0:3]=="run"):
    rfile=open("unifit_run",'w')
    for rline in sys.stdin:
      if (rline[0:3].lower()=="end"):
        break
      rfile.write(rline)
    rfile.close()
    continue

# definitions are expanded into act_uni_inp1:
  try:
    definition=defdict[stmt]
  except:
    pass
  else:
    for defline in definition:
      expline=""
      for defarg in defline:
        if (defarg[0]=="$"):
          try:
            pnum=int(defarg[1:])
          except:
            expline=expline+defarg+" "
          else:
            if (pnum<len(defline)):
              expline=expline+cargs[pnum]+" "
            else:
              expline=expline+" "
        else:
          expline=expline+defarg+" "
      aui1.write(expline+"\n")
    continue

# All other commands are copied into act_uni_inp1
  aui1.write(line)

  if (stmt[0:3]=="par"):
    parnam.append(cargs[2])
    # print(cargs[2])
    continue

  if (stmt[0:3]=="for"):
    ffile=line[:-1].split(maxsplit=1)[1]
    # print(ffile)
    continue

  if (stmt[0:4]=="comp"):
    forc=line[:-1].split(maxsplit=1)[1]
    # print(forc)
    continue

  if (stmt[0:3]=="opt"):
    opts=opts+line[:-1].split(maxsplit=1)[1]+" "
    # print(opts)
    continue

  if (stmt[0:3]=="sub"):
    sfile.append(line[:-1].split(maxsplit=1)[1])
    # print(sfile)
    continue

  if (stmt[0:4]=='init'):
    subini=line[:-1].split(maxsplit=1)[1]
    # print(subini)
    continue

# close expanded actual unifit input:
aui1.close()

# generate FORTRAN compilable module:

funmod=open("funmod.f",'w')

# universal header:
funmod.write("""      real*8 function fitfun(x)

      implicit real*8 (a-h,o-z)
      dimension xdel(20),wdel(20)
      real*8 x
""")

# parameter declarations and mapping to common:
for par in parnam:
  funmod.write("     &       ,"+par+"\n")
funmod.write("      common/fitpar/ npar,idf,newpar,idel,xdel,wdel,aconv,bconv\n")
for par in parnam:
  funmod.write("     &               ,"+par+"\n")
funmod.write("!$OMP THREADPRIVATE(/fitpar/)\n\n")

# optional external FORTRAN file:
if ffile:
  try:
    exf=open(udir+"/"+ffile,'r')
  except:
    print("Fortran file "+ffile+" does not exist")
    sys.exit(101)
  for fline in exf:
    funmod.write(fline)
  exf.close()

# (mandatory) inline FORTRAN code:
funmod.write(code)
funmod.write("""
      fitfun=y
      return
      end

""")

# optional initialisation subroutine:
funmod.write("      subroutine fn_ini\n")
if subini:
  funmod.write("      call "+subini+"\n")
funmod.write("""      return
      end


""")

# optional external subroutine files:
for sfname in sfile:
  try:
    exsf=open(udir+"/"+sfname,'r')
  except:
    print("Subroutine file "+sfname+" does not exist")
    sys.exit(102)
  for fline in exsf:
    funmod.write(fline)
  exsf.close()

funmod.close()

# generate FORTRAN compilation command:

mfile=open("make_p2",'w')
mfile.write(forc+" "+opts+" unifit_p2.f hslsub.f kova.f funmod.f -o unifit_p2 -I '"+udir+"'\n")
mfile.close()
