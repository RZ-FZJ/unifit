#!/usr/bin/env python3

# Look up a regex in the unfit history and output the corresponding history section

import sys
# import math
# import numpy as np
# import os
import argparse
import re

parser=argparse.ArgumentParser(description="History miner for unifit")
# parser.add_argument("-X","--xyzzy",type=int,default=1,help="... [1]")
# parser.add_argument("-Y","--yzzyx",action='store_true',help="...")
# parser.add_argument("-R","--range",nargs=2,metavar=('XMIN','XMAX'),type=float,help="...")
parser.add_argument("regex",nargs='?',help="regex to find in unifit history")
parser.add_argument("-P","--para",action='store_true',help="extract fit results only (content of unifit_para.txt)")
parser.add_argument("-C","--command",action='store_true',help="extract command file only")
parser.add_argument("-N","--line",type=int,default=-1,help="extract history block at line number LINE")
parser.add_argument("-M","--menu",action='store_true',help="show menu of matches instead of taking last")

args=parser.parse_args()
regex=args.regex
para=args.para
command=args.command
menu=args.menu
nline=args.line
if regex is None and nline<0:
  print("Either line number (-N, --line) or regex required.",file=sys.stderr)
  sys.exit(2)
if regex is not None and nline>=0:
  print("Warning: -N, --line overrides regex search.",file=sys.stderr)
if menu:
  print("-M, --menu not yet implemented.",file=sys.stderr)
  sys.exit(2)

hist=open("unifit.history",'r')
histlist=list(hist)
lhist=len(histlist)

if (nline<0):
  hitlist=[]
  for i,line in enumerate(histlist):
    if re.search(".*"+regex+".*",line):
      hitlist.append(i)
  nhits=len(hitlist)
  if nhits==0:
    print("No matches found.",file=sys.stderr)
    sys.exit(1)
  else:
    if nhits>1:
      print("%i matches found, taking last." % (nhits),file=sys.stderr)
    i=hitlist[-1]
else:
  i=nline-1

histblock=[]
while not "==========" in histlist[i]:
  i=i-1
i=i+1
while i<lhist and not "==========" in histlist[i]:
  histblock.append(histlist[i][:-1])
  i=i+1

if para:
  i=0
  while not "Fit results" in histblock[i]:
    i=i+1
  for line in histblock[i+1:]:
    print(line)
elif command:
  i=3
  while not "Fit results" in histblock[i]:
    print(histblock[i])
    i=i+1
else:
  for line in histblock:
    print(line)