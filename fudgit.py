#!/usr/bin/python

import os

parafile=open('unifit_para.txt','r')

parafile.readline()
parafile.readline()
parafile.readline()

pline=parafile.readline()
while (pline!='\n'):
  nfiles=int(pline[0:2])
  pline=parafile.readline()

ff=[]
for i in range(0,nfiles):
  ff.append(1.0)

while (pline!=''):
  if (pline[0:5]=='fudge'):
    ff[int(pline[32:34])-1]=float(pline[36:46])
  pline=parafile.readline()
  
for i in range(0,nfiles):
  fname='unifit_fitf_'+str(101+i)[1:3]
  ffname=fname+'_f'
  print "awk '{ print $1,$2/"+str(ff[i])+" }' "+fname+" > "+ffname
  os.system("awk '{ print $1,$2/"+str(ff[i])+" }' "+fname+" > "+ffname)
  dname='unifit_data_'+str(101+i)[1:3]
  dfname=dname+'_f'
  print "awk '{ print $1,$2/"+str(ff[i])+",$3/"+str(ff[i])+" }' "+dname+" > "+dfname
  os.system("awk '{ print $1,$2/"+str(ff[i])+",$3/"+str(ff[i])+" }' "+dname+" > "+dfname)
  
