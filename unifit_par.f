C     parameters for all modules (may depend on implementation):

C     Default compiler and options:
      character*200 dforc,dopts
      parameter(dforc='gfortran',dopts='')

C     Special numbers:
      parameter(amagic=4.68342354d37)
      parameter(maxint=2147483647)

C     Maximal values: linelength<200, namelength<50, parameters<50,
C                     subroutine files<20, inline code lines<1000,
C                     inline code line length<80, gnuplot commands<50
C                     parameters-to-fit<350, total data points<25000,
C                     data points per file<5000
C                     (these values are also limited by fitsub.f),
C                     files<50, calculated points<10001, columns in output<60,
C                     delta functions<20, resolution points<1000,
      parameter(mpar=50,mfile=100)
      parameter(mptf=350,mdat=5000,mdatt=25000)
      parameter(micl=1000,msfile=20,mgcom=50,mint=10001)
      parameter(mres=5000,mdel=20,mcolo=60)
      parameter(mw=2*mdatt*mptf+2*mptf*mptf+2*mdatt+5*mptf)

