#!/usr/bin/env python

# Analyse unifit bootstrap results

# import sys
# import math
import numpy as np
import matplotlib.pyplot as plt
# import os
# import argparse
from itertools import chain
import re

# parser=argparse.ArgumentParser(description="...")
# parser.add_argument("-X","--xyzzy",type=int,default=1,help="... [1]")
# parser.add_argument("-Y","--yzzyx",action='store_true',help="...")
# parser.add_argument("-R","--range",nargs=2,metavar=('XMIN','XMAX'),type=float,help="...")
# parser.add_argument("infile",nargs='?',help="input file...")

# args=parser.parse_args()
# xyzzy=args.xyzzy


def parse_range(rng):
    parts = rng.split('-')
    if 1 > len(parts) > 2:
        raise ValueError("Bad range: '%s'" % (rng,))
    parts = [int(i) for i in parts]
    start = parts[0]
    end = start if len(parts) == 1 else parts[1]
    if start > end:
        end, start = start, end
    return range(start, end + 1)


def parse_range_list(rngs):
    return sorted(set(chain(*[parse_range(rng) for rng in rngs.split(',')])))


def fpar(par):
  if (par[1]=="*"):
    return par[0]
  else:
    return ("%s(%s)" % (par[0],par[1]))


# This code works with the old version of unifit_p2 but does not read the classic errors
# paraf=open("unifit_para.txt")
#
# inpars=False
# parlist=[]
# for pline in paraf:
#   if inpars:
#     plines=pline.split()
#     if not (plines[3]=="const"):
#       parlist.append(plines)
#   if (pline[0:11]=="Parameters:"):
#     inpars=True

parafn=open("unifit_noboot.txt")

parlist=[]
grouplist=[]
for pline in parafn:
  plines=pline.split()
  if (plines[2]==":"):
    group=plines[1]
    if (not(group in grouplist)):
      grouplist.append(group)
      parlist.append(plines)
  else:
    parlist.append(plines)

for parnum,par in enumerate(parlist,1):
  print("%4i: %s" % (parnum,fpar(par)))

# It is assumed that the first data line is just the ideal fit:
# bootdata=np.loadtxt("unifit_boot.txt")[1:,:]
# Unfortunately, three-digit exponents as 1.234+123 break this,
# need to code expliditly:
bootdf=open("unifit_boot.txt",'r')
bdflist=[]
for bfline in bootdf:
  if (bfline[0]!="#"):
    fline=[]
    for bfitem in bfline.split():
      mbfitem=re.match('(.*\d)([+-]\d*)',bfitem)
      if mbfitem:
        fixed_item=mbfitem.group(1)+"E"+mbfitem.group(2)
        # print(bfitem,fixed_item)
      else:
        fixed_item=bfitem
      fline.append(float(fixed_item))
    bdflist.append(fline)
bootdata=np.asarray(bdflist[1:])
bootdf.close()

nboot=np.shape(bootdata)[0]
print("%i bootstrap replicas" % nboot)

qlog=input("Logarithmic parameters (e.g. 1-4,7,9)? ")
if (qlog==""):
  loglist=[]
else:
  loglist=parse_range_list(qlog)
  # print(loglist)

ofile=open("bootvar.txt",'w')
for ip,par in enumerate(parlist):
  print(par)
  pclass=float(parlist[ip][-2])
  if ip+1 in loglist:
    pboot=np.exp(np.average(np.log(bootdata[:,ip])))
    dpboot1=pclass*np.sqrt(np.sum((np.log(bootdata[:,ip])-np.log(pclass))**2)/nboot)
    print(bootdata[:,ip],pboot)
    dpboot2=pboot*np.sqrt(np.sum((np.log(bootdata[:,ip])-np.log(pboot))**2)/nboot)
  else:
    pboot=np.average(bootdata[:,ip])
    dpboot1=np.sqrt(np.sum((bootdata[:,ip]-pclass)**2)/nboot)
    dpboot2=np.sqrt(np.sum((bootdata[:,ip]-pboot)**2)/nboot)
  ofile.write("%11.4e %11.4e %11.4e\n" % (pboot,dpboot1,dpboot2))
ofile.close()

while True:

  print("""
  1: Overview
  2: Confidence Intervals
  3: Analyse single parameter
  4: Histogram of parameter
  5: Plot two parameters
  0: Exit""")
  act=input("Action? ")

  if ((act=="0") or (act=="")):
    break

  if (act=="1"):
    print("#    Parameter                     Value  Classic Error  Boot Var    Deviation")
    for ip,par in enumerate(parlist):
      pclass=float(parlist[ip][-2])
      dpclass=float(parlist[ip][-1])
      if ip+1 in loglist:
        dpboot=pclass*np.sqrt(np.sum((np.log(bootdata[:,ip])-np.log(pclass))**2)/nboot)
        dev=100.0*np.log(dpclass/dpboot)
        print("%-5i%-20s L  %11.4e    %11.4e %11.4e     %6.1f" % (ip+1,fpar(par),pclass,dpclass,dpboot,dev))
      else:
        dpboot=np.sqrt(np.sum((bootdata[:,ip]-pclass)**2)/nboot)
        dev=100.0*np.log(dpclass/dpboot)
        print("%-5i%-20s    %11.4e    %11.4e %11.4e     %6.1f" % (ip+1,fpar(par),pclass,dpclass,dpboot,dev))

  elif (act=="2"):
    qcl=input("Confidence level [0.683]")
    print("#    Parameter                  Value     Confidence Interval")
    ofile=open("bootconf.txt",'w')
    climlist=[]
    if (qcl==""):
      clevel=0.683
    else:
      clevel=float(qcl)
    for ip,par in enumerate(parlist):
      pclass=float(parlist[ip][-2])
      climits=np.quantile(bootdata[:,ip],[0.5*(1.0-clevel),0.5*(1.0+clevel)])
      print("%-5i%-20s %11.4e %11.4e..%-11.4e" % (ip+1,fpar(par),pclass,climits[0],climits[1]))
      ofile.write("%11.4e %11.4e %11.4e\n" % (pclass,climits[0],climits[1]))
      climlist.append(climits)
    ofile.close()

  elif (act=="3"):
    while True:
      qparnum=input("Parameter number (0: end)? ")
      if ((qparnum=="0") or (qparnum=="")):
        break
      parnum=int(qparnum)
      ip=parnum-1
      pclass=float(parlist[ip][-2])
      dpclass=float(parlist[ip][-1])
      if parnum in loglist:
        pboot=np.exp(np.average(np.log(bootdata[:,ip])))
        dpboot1=pclass*np.sqrt(np.sum((np.log(bootdata[:,ip])-np.log(pclass))**2)/nboot)
        dpboot2=pboot*np.sqrt(np.sum((np.log(bootdata[:,ip])-np.log(pboot))**2)/nboot)
      else:
        pboot=np.average(bootdata[:,ip])
        dpboot1=np.sqrt(np.sum((bootdata[:,ip]-pclass)**2)/nboot)
        dpboot2=np.sqrt(np.sum((bootdata[:,ip]-pboot)**2)/nboot)
      print("Classic value:                    %11.4e" % pclass)
      print("Bootstrap average value:          %11.4e" % pboot)
      print("Classic error:                    %11.4e" % dpclass)
      print("Bootstrap error (from classic):   %11.4e" % dpboot1)
      print("Bootstrap error (from bootstrap): %11.4e" % dpboot2)
      if climlist:
        print("Confidence limits:                %11.4e..%-11.4e" % (climlist[ip][0],climlist[ip][1]))

  elif (act=="4"):
    while True:
      qparnum=input("Parameter number (0: end)? ")
      if ((qparnum=="0") or (qparnum=="")):
        break
      parnum=int(qparnum)
      ip=parnum-1
      if (parnum in loglist):
        # This is a quick but dirty way for log histograms:
        hdata=np.log10(bootdata[:,ip])
        label=("Log10(%s)" % fpar(parlist[ip]))
      else:
        hdata=bootdata[:,ip]
        label=fpar(parlist[ip])
      bins,edges,patches=plt.hist(hdata, bins='auto')
      ymax=np.max(bins)
      if (parnum in loglist):
        pclass=np.log10(float(parlist[ip][-2]))
      else:
        pclass=float(parlist[ip][-2])
      plt.xlabel(label)
      plt.plot([pclass,pclass],[0,1.1*ymax])
      if climlist:
        if (parnum in loglist):
          plt.plot([np.log10(climlist[ip][0]),np.log10(climlist[ip][0])],[0,0.9*ymax])
          plt.plot([np.log10(climlist[ip][1]),np.log10(climlist[ip][1])],[0,0.9*ymax])
        else:
          plt.plot([climlist[ip][0],climlist[ip][0]],[0,0.9*ymax])
          plt.plot([climlist[ip][1],climlist[ip][1]],[0,0.9*ymax])
      plt.show()

  elif (act=="5"):
    while True:
      qparnum=input("Parameter numbers (0: end)? ")
      if ((qparnum=="0") or (qparnum=="")):
        break
      ip1=int(qparnum.split()[0])-1
      ip2=int(qparnum.split()[1])-1
      fig,ax=plt.subplots()
      if ((ip1+1) in loglist):
        ax.set_xscale('log')
      if ((ip2+1) in loglist):
        ax.set_yscale('log')
      ax.plot(bootdata[:,ip1],bootdata[:,ip2],'o')
      ax.plot(float(parlist[ip1][-2]),float(parlist[ip2][-2]),'ro')
      ax.set_xlabel(fpar(parlist[ip1]))
      ax.set_ylabel(fpar(parlist[ip2]))
      plt.show()

