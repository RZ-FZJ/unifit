#!/usr/bin/env python3

# V 2: create set commands if necessary

import sys,getopt
import argparse

parser=argparse.ArgumentParser(description="Patch unifit command file from fit results")
parser.add_argument("-P","--parameter_file",type=str,default="",help="parameter file for patching")
parser.add_argument("-T","--test",action="store_true",help="show the internals")
parser.add_argument("input_file",nargs='?',help="original unifit command file (default='unifit.input')")
parser.add_argument("output_file",nargs='?',help="patched unifit command file (default=input_file, overwriting!)")
parser.add_argument("pfile",nargs='?',help="parameter file for patching (default='unifit_para.txt')")

args=parser.parse_args()

if (args.input_file):
  ufname=args.input_file
else:
  ufname='unifit.input'
if (args.output_file):
  nfname=args.output_file
else:
  nfname=ufname
if (args.pfile):
  pfname=args.pfile
else:
  pfname='unifit_para.txt'
if (args.parameter_file):
  pfname=args.parameter_file
test=args.test

com=[]
with open(ufname) as ufile:
  for line in ufile:
    com.append(line)

coms=[]
for line in com:
  coms.append(line.split())

parlist=[]
setlist=[]
grlist=[]
nf=0
skip=False
for ln,sline in enumerate(coms):
  if len(sline)>0:
    if not skip:
      skip=( sline[0].lower().startswith("inline") or
             sline[0].lower().startswith("run") or
             sline[0].lower().startswith("gnuplot") )
      if sline[0].lower().startswith("par"):
        paritem=[ln]
        paritem.extend(sline)
        parlist.append(paritem)
      if sline[0].lower().startswith("fil"):
        nf+=1
      if sline[0].lower().startswith("set"):
        setitem=[ln]
        setitem.append(nf)
        setitem.extend(sline)
        setlist.append(setitem)
      if (sline[0].lower().startswith("gr") and sline[2].lower()!="c"):
        gritem=[ln]
        gritem.append(nf)
        gritem.extend(sline)
        grlist.append(gritem)
    else:
      skip = not sline[0].lower().startswith("end")

fitlist=[]
with open(pfname) as pfile:
  for line in pfile:
    if line.startswith("Parameters:"):
      break
  for line in pfile:
    fititem=['']
    fititem.extend(line.split())
    if fititem[4]!="const":
      fitlist.append(fititem)

if test:
  print("parlist =",parlist)
  print("setlist =",setlist)
  print("grlist  =",grlist)
  print("fitlist =",fitlist,"\n")

ncom=com[:]

setmissing=False
grmissing=False

for par in parlist:

  if par[2].lower()=="f":
    for findex,fit in enumerate(fitlist):
      if fit[1]==par[3].lower():
        coms[par[0]][3]=fit[3]
        ncom[par[0]]=" ".join(coms[par[0]])+"\n"
        fitlist[findex][0]=False
        break

  if par[2].lower()=="i":
    for findex,fit in enumerate(fitlist):
      if fit[1]==par[3].lower():
        found=False
        for set in setlist:
          if (fit[1]==set[3].lower() and int(fit[2])==set[1]):
            coms[set[0]][2]=fit[3]
            ncom[set[0]]=" ".join(coms[set[0]])+"\n"
            found=True
            break
        if not found:
          setmissing=True
          fitlist[findex][0]='s'

  if par[2].lower()=="g":
    for findex,fit in enumerate(fitlist):
      if fit[1]==par[3].lower():
        found=False
        for group in grlist:
          if (fit[1]==group[3].lower() and int(fit[4])==group[1] and len(group)>=6):
            coms[group[0]][3]=fit[5]
            ncom[group[0]]=" ".join(coms[group[0]])+"\n"
            found=True
            break
        if not found:
          grmissing=True
          fitlist[findex][0]='g'

if setmissing:
  qset=input("One or more set commands missing. Create them? ")
  if qset[0].lower()=="y":
    for fititem in fitlist:
      if fititem[0]=='s':
        if test:
          print(">",fititem)
        nf=0
        for ln,line in enumerate(ncom):
          if line[0:3].lower()=="fil":
            nf+=1
          if int(fititem[2])==nf:
            ncom.insert(ln+1,"set "+fititem[1]+" "+fititem[3]+"\n")
            break

if grmissing:
  qgroup=input("One or more group without value. Add value? ")
  if qgroup[0].lower()=="y":
    for fititem in fitlist:
      if fititem[0]=='g':
        if test:
          print(">",fititem)
        nf=0
        for ln,line in enumerate(ncom):
          if line[0:3].lower()=="fil":
            nf+=1
          if (int(fititem[4])==nf and line[0:2]=="gr"):
            ncom[ln]=line[:-1]+" "+fititem[5]+"\n"
            break

with open(nfname,'w') as nfile:
  for line in ncom:
    nfile.write(line)
  nfile.close()

