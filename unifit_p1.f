      program unifit_p1

C     MARK 2

C     This is pass 1 of the UNIFIT package:
C     Parameter names are parsed from the standard input file
C     and combined with the user defined function
C     to a compilable module.

C     Maximal values from include file:

      include "unifit_par.f"

      character*250 append
      character*200 line,tail,opts,forc,udir,subini
      character*200 cline,code(micl)
      character*50 parnam(mpar),parse,stmt,cgname
      character*50 ffile,sfile(msfile),aconv

C     read where is the userdir and the control file name:

      read(*,'(a)',end=10) udir
      read(*,'(a)',end=10) cfname

C     read the input:

      npar=0
      iexff=0
      nexsf=0
      ncode=0
      iconv=0
      icomm=0
      opts=dopts
      forc=dforc
      subini=""
 5    read(*,'(a)',end=10) line
      stmt=parse(line,1)

      if (icomm.ne.0) then
        if (stmt(1:4).eq.'endc') icomm=0
        goto 5
      end if

      if (stmt(1:4).eq.'comm') icomm=1

      if (stmt(1:3).eq.'par') then
        npar=npar+1
        parnam(npar)=parse(line,3)
      end if

      if (stmt(1:3).eq.'for') then
        iexff=1
        ffile=parse(line,2)
      end if

      if (stmt(1:4).eq.'comp') then
        forc=tail(line)
      end if

      if (stmt(1:3).eq.'opt') then
        lopts=length(opts)
        opts(lopts+2:200)=tail(line)
      end if

      if (stmt(1:3).eq.'sub') then
        nexsf=nexsf+1
        sfile(nexsf)=parse(line,2)
      end if

      if (stmt(1:6).eq.'inline') then
 45     read(*,'(a)',end=10) cline
        if (cline(1:3).eq.'end') goto 40
        ncode=ncode+1
        code(ncode)=cline
        goto 45
 40     continue
      end if

      if (stmt(1:3).eq.'run') then
        open(27,file='unifit_run',status='unknown')
 55     read(*,'(a)',end=10) line
        if (line(1:3).eq.'end') goto 50
        write(27,*) line
        goto 55
 50     close(27)
      end if

      if (stmt(1:4).eq.'init') then
        subini=tail(line)
      end if

C     if (stmt(1:3).eq.'cfi') then
C       if (iconv.eq.0) iconv=1
C     end if
C
C     if (stmt(1:2).eq.'cm') then
C       aconv=parse(line,2)
C       if (aconv(1:1).eq.'p') iconv=1
C       if (aconv(1:1).eq.'n') iconv=2
C       if (aconv(1:1).eq.'a') iconv=3
C     end if

      goto 5

C     generate compilable module:

 10   open(20,file='funmod.f',status='unknown')

      write(20,'(a)')    '      real*8 function fitfun(x)'
      write(20,'(a)')
      write(20,'(a)')    '      implicit real*8 (a-h,o-z)'
      write(20,'(a)')    '      dimension xdel(20),wdel(20)'

      write(20,'(a)')    '      real*8 x'
      do ipar=1,npar
        write(20,'(2a)') '     &       ,',parnam(ipar)
      end do

      write(20,'(a)')
     &'      common/fitpar/ npar,idf,newpar,idel,xdel,wdel,aconv,bconv'
      do ipar=1,npar
        write(20,'(2a)') '     &               ,',parnam(ipar)
      end do
      write(20,'(a)') '!$OMP THREADPRIVATE(/fitpar/)'
      write(20,*)

      if (iexff.ne.0) then
C       write(20,'(3a)') '      include ''',ffile,''''
        open(25,file=append(udir,ffile),status='old',err=29)
 22     read(25,'(a)',end=25) cline
        write(20,'(a)') cline
        goto 22
 29     write(*,*) 'Fortran file ',ffile,'does not exist!'
        call exit(101)
 25     close(25)
      end if

      do icode=1,ncode
        write(20,'(a)')  code(icode)
      end do
      write(20,*)


      write(20,'(a)')    '      fitfun=y'
      write(20,'(a)')    '      return'
      write(20,'(a)')    '      end'
      write(20,*)
      write(20,*)

      write(20,'(a)')    '      subroutine fn_ini'
      if (subini.ne."") then
        write(20,'(2a)') '      call ',subini
      end if
      write(20,'(a)')    '      return'
      write(20,'(a)')    '      end'
      write(20,*)
      write(20,*)

      do iexsf=1,nexsf
C       write(20,'(3a)') '      include ''',sfile(iexsf),''''
C       write(*,*) '|',udir,'|',sfile(iexsf),'|'
C       write(*,*) '|',append(udir,sfile(iexsf)),'|'
        open(25,file=append(udir,sfile(iexsf)),status='old',err=39)
 32     read(25,'(a)',end=35) cline
        write(20,'(a)') cline
        goto 32
 39     write(*,*) 'Subroutine file ',sfile(iexsf),'does not exist!'
        call exit(102)
 35     close(25)
      end do

      close(20)

C     generate FORTRAN compilation command:

      open(21,file='make_p2',status='unknown')
      write(21,'(4a)') forc,opts,
     &  ' unifit_p2.f hslsub.f kova.f funmod.f -o unifit_p2 -I ',
     &  udir
      close(21)

      end


      character*50 function parse(line,nparq)

C     returns the nparq-th component of line (separated by blank space)
C     converted to lowercase

      character*200 line
      character*50 param
      character och

      ich0=1
      npar=1

C     skip blanks

 10   do ich=ich0,200
       if (line(ich:ich).ne.' ') goto 20
      end do
      goto 99
 20   ich0=ich

C     read parameter

      jch=1
      param=' '
      do ich=ich0,200
        if (line(ich:ich).ne.' ') then
          och=line(ich:ich)
          if (('A'.le.och).and.(och.le.'Z')) then
            param(jch:jch)=char(ichar(och)+32)
          else
            param(jch:jch)=och
          end if
          jch=jch+1
          if (jch.gt.50) goto 99
        else
          goto 30
        end if
      end do
      goto 99
 30   if (npar.eq.nparq) goto 99
      npar=npar+1
      ich0=ich
      goto 10

 99   parse=param
      return
      end


      character*200 function tail(line)

      character*200 line,tail1

      ich0=1

C     skip blanks

 10   do ich=ich0,200
        if (line(ich:ich).ne.' ') goto 20
      end do
      tail1=''
      goto 99

 20   ich0=ich

C     overread first parameter

      do ich=ich0,200
        if (line(ich:ich).eq.' ') goto 30
      end do
      tail1=''
      goto 99

 30   tail1=line(ich+1:200)

 99   tail=tail1
      return
      end


      integer function length(string)

      character*(*) string

      do 15,i=len(string),1,-1
        if (string(i:i).ne.' ') goto 20
 15   continue

 20   length=i

      return
      end


      character*250 function append(dir,file)

      character*250 res
      character*200 dir
      character*50 file

C     search first blank

 10   do ich=1,200
       if (dir(ich:ich).eq.' ') goto 20
      end do
      goto 99

 20   res(1:200)=dir
      res(ich:ich)='/'
      res(ich+1:250)=file
C     write(*,*) 'APPEND:'
C     do i=1,80
C       write(*,*) i,':',res(i:i)
C     end do
      append=res

 99   return
      end
