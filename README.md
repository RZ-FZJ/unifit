# UNIFIT

UNIFIT is a highly flexible program for fitting theories to experimental data.
The intended use is for theories which cannot be simply stated as single-line formula. It also offers a couple of features which are often neglected in commercial software like multiple data files, fixing and grouping of parameters, convolution with an instrumental resolution, bootstrap error estimates, Monte Carlo search for starting values, use of OpenMP etc.

The program executes two passes on a command file given by the user. In the first (python) pass a compilable module containing the user function is created. This module is compiled (FORTRAN) and executed in the second pass. In that way, nearly every theory which can be written as a set of FORTRAN subroutines can be implemented and compiled on the fly.

The fitting engine is the routine VA05 from the Harwell scientific library (HSL). The method is a compromise between three different algorithms for minimising a sum of squares, namely
Newton-Raphson, Steepest Descent and Marquardt. In addition the routine KOVA is used to obtain error estimates on the fit parameter. This routine was written by IFF at Forschungszentrum Juelich. Because VA05 and other HSL routines used are licensed for academic use but not for redistribution they are not included in the package but have to be downloaded by the user. A detailed description of the requirements for installation is included in the documentation.

## Quick Start

Although it is highly recommended to read the documentation `unifit.pdf`, here are the essential steps to get started for the impatient:

1. Download this package.
1. Edit the paths where UNIFIT is installed and to gnuplot in the python script `unifit.py`.
1. Download the routines VA05, MA10, QA05, KB07, FD05 from <http://www.hsl.rl.ac.uk>.
1. Run `make_hslsub`.
1. Test: `unifit.py -c`
