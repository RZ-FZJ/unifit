      program unifit_p2

C     MARK 2

C     This is pass 2 of the UNIFIT package:
C     Here the actual fit is done.

      implicit real*8 (a-h,o-z)

C     Maximal values are all defined in the following include:

      include "unifit_par.f"

      character*80 cline,code(micl)
      character*1024 line,dline,gline,gcom(mgcom),pline
      character*50 parnam(mpar),parse,parse1,stmt,ffile,sg,sfile(msfile)
      character*50 fnam(mfile),arg,eparn,rfnam(mfile),pfile,pnam,idfstr
      character*50 grlab(mpar,mfile),nextgrl,ngrlra,pboot,udir,cfname
      character*50 oboot,fn
      character*31 fnc
      character*14 dfname,bdname
      character*13 rfname
      character*1 bsl,ch
      character partyp(mpar),party
      dimension par(mpar),parini(mpar,mfile),parvar(mpar,mfile)
      dimension parmin(mpar,mfile),parmax(mpar,mfile)
      dimension isca(mpar,mfile),asca(6,mpar,mfile)
      dimension igroup(mpar,mfile),ng(mpar),igc(mpar)
      dimension respar(mpar,mfile),errpar(mpar,mfile),par1(mpar)
      dimension bpar(mpar,mfile),dev2(mpar,mfile)
      dimension ptf(mptf),dptf(mptf),ptf0(mptf)
      dimension icolx(mfile),icoly(mfile),icole(mfile),ndpf(mfile)
      dimension icolrx(mfile),icolry(mfile)
      dimension ibrow(mfile),ierow(mfile)
      dimension ibrowr(mfile),ierowr(mfile)
      dimension xd2(mfile,mdat),yd2(mfile,mdat),ed2(mfile,mdat)
      dimension xmin(mfile),xmax(mfile),ymin(mfile),ymax(mfile)
      dimension xrmin(mfile),xrmax(mfile),yrmin(mfile),yrmax(mfile)
      dimension xd(mdatt),yd(mdatt),ed(mdatt),swd(mdatt),idf(mdatt)
      dimension xc(mfile,mint),yc(mfile,mint)
      dimension xt(mfile,mint),yt(mfile,mint)
      dimension xr(mres,mfile),yr(mres,mfile),idr(mfile),nres(mfile)
      dimension indr(mres),xru(mres),yru(mres)
      dimension iconv(mfile),inono(mfile)
      dimension w(mw),f(mdatt),wd(mdatt),fct(mdatt)
      dimension d1(mdel),w1(mdel)
      dimension icospc(mcolo)
      dimension fw(mdatt),pw(mdatt),pw0(mdatt),indx(mdatt),iwork(mdatt)
      dimension errab2(mfile),errsqt(mfile),errrel(mfile)
      dimension yd0(mdatt),fct0(mdatt)

C     data and resolution values for CALFUN:
      common/dat/ xd,yd,ed,swd,idf,iweight,npar,nfile,nfun,
     &  itrace,ipgi,imoni
      common/ff/ fct
      common/conv/ xr,yr,nres,iconv,idr,inono,mflag
C     This common corresponds to INDPAR, PTF2PAR, and DPTF2PA:
      common/pardef/ asca,isca,parini,parvar,parnam,igroup,partyp
C     This common corresponds to FITFUN:
      common/fitpar/ npar1,idf1,newpar,idel,d1,w1,aconv,bconv,par1
!$OMP THREADPRIVATE(/fitpar/)

C     Set the defaults and constant variables:

      bsl=char(92)
      npar=0
      nfile=0
      iweight=0
      dstep=1.d-5
      dmax=1.0
      acc=1.d-6
      maxfun=1000
      ipr0=0
      ishda=0
      ishpa=0
      iplot=0
      ierrpl=0
      iclog=0
      icalc=0
      itheo=0
      ngcom=0
      iodat=0
      iofit=0
      iothe=0
      iplth=0
      ikova=0
      iva05=0
      ncolo=0
      iplogx=0
      iplogy=0
      icomm=0
      xpmin=0.0
      xpmax=0.0
      ypmin=0.0
      ypmax=0.0
      itrace=0
      ncode=0
      iexff=0
      nexsf=0
      ndr=0
      ilft=0
      xoff=0.0
      yoff=0.0
      irplot=0
      irlogx=0
      xrpmin=0.0
      xrpmax=0.0
      yrpmin=0.0
      yrpmax=0.0
      icont=0
      iresi=0
      idepa=0
      itboot=0
      itlog=0
      ibdiag=0
      ierrm=0
      imc=0
      ipgi=0
      iconv1=0
      isconv=0
      imoni=0

      do ipar=1,mpar
        ng(ipar)=0
        igc(ipar)=0
      end do

      do ifile=1,mfile
        xmin(ifile)=amagic
        xmax(ifile)=amagic
        ymin(ifile)=amagic
        ymax(ifile)=amagic
        xrmin(ifile)=amagic
        xrmax(ifile)=amagic
        yrmin(ifile)=amagic
        yrmax(ifile)=amagic
        idr(ifile)=1
        iconv(ifile)=0
        inono(ifile)=0
        errab2(ifile)=0.0
        errsqt(ifile)=0.0
        errrel(ifile)=0.0
        do ipar=1,mpar
          grlab(ipar,ifile)=''
        end do
      end do

C     read where is the userdir and the control file name:

      read(*,'(a)',end=10) udir
      read(*,'(a)',end=10) cfname

C     read the rest of the fit control file:

 5    read(*,'(a)',end=10) line
      stmt=parse(line,1)
C     write(*,*) stmt

      if (icomm.ne.0) then
        if (stmt(1:4).eq.'endc') icomm=0
        goto 5
      end if

      if (stmt(1:4).eq.'comm') icomm=1

      if (stmt(1:3).eq.'par') then
        npar=npar+1
        party=parse(line,2)
        partyp(npar)=party
        if ((party.eq."p").or.(party.eq."g").or.(party.eq."i")) ipgi=1
        parnam(npar)=parse(line,3)
        parva1=1.0
        parmi1=amagic
        parma1=amagic
          call rdpar(line,4,parin1,parva1,parmi1,parma1)
        do ifile=1,mfile
          parini(npar,ifile)=parin1
          parvar(npar,ifile)=parva1
          parmin(npar,ifile)=parmi1
          parmax(npar,ifile)=parma1
        end do
      end if

      if (stmt(1:6).eq.'inline') then
 45     read(*,'(a)',end=10) cline
        if (cline(1:3).eq.'end') goto 40
        ncode=ncode+1
        code(ncode)=cline
C       write(*,*) "Code line: ",ncode
        goto 45
 40     continue
      end if

      if (stmt(1:3).eq.'for') then
        iexff=1
        ffile=parse1(line,2)
      end if

      if (stmt(1:3).eq.'sub') then
        nexsf=nexsf+1
        sfile(nexsf)=parse1(line,2)
      end if

      if (stmt(1:7).eq.'gnuplot') then
 75     read(*,'(a)',end=10) gline
C       write(*,*) gline
        if (gline(1:3).eq.'end') goto 70
        ngcom=ngcom+1
        gcom(ngcom)=gline
        goto 75
 70     continue
      end if

      if (stmt(1:3).eq.'fil') then
        ilft=0
        nfile=nfile+1
        fnam(nfile)=parse1(line,2)
C       write(*,*) '|',fnam(nfile),'|'
        icolx(nfile)=1
        icoly(nfile)=2
        icole(nfile)=3
        ibrow(nfile)=1
        ierow(nfile)=maxint
        if (iconv1.ne.0) iconv(nfile)=iconv1
      end if

      if (stmt(1:3).eq.'col') then
        if (ilft.eq.0) then
          icolx(nfile)=val(parse(line,2))
          icoly(nfile)=val(parse(line,3))
          icole(nfile)=val(parse(line,4))
        else
          icolrx(ndr)=val(parse(line,2))
          icolry(ndr)=val(parse(line,3))
        end if
      end if

      if (stmt(1:3).eq.'row') then
        if (ilft.eq.0) then
          ibrow(nfile)=val(parse(line,2))
          if (val(parse(line,3)).ne.0.0) ierow(nfile)=val(parse(line,3))
        else
          ibrowr(ndr)=val(parse(line,2))
          if (val(parse(line,3)).ne.0.0) ierowr(ndr)=val(parse(line,3))
        end if
      end if

      if (stmt(1:4).eq.'xmin') then
        if (ilft.eq.0) then
          xmin(nfile)=val(parse(line,2))
        else
          xrmin(nfile)=val(parse(line,2))
        end if
      end if

      if (stmt(1:4).eq.'xmax') then
        if (ilft.eq.0) then
          xmax(nfile)=val(parse(line,2))
        else
          xrmax(nfile)=val(parse(line,2))
        end if
      end if

      if (stmt(1:4).eq.'ymin') then
        if (ilft.eq.0) then
          ymin(nfile)=val(parse(line,2))
        else
          yrmin(nfile)=val(parse(line,2))
        end if
      end if

      if (stmt(1:4).eq.'ymax') then
        if (ilft.eq.0) then
          ymax(nfile)=val(parse(line,2))
        else
          yrmax(nfile)=val(parse(line,2))
        end if
      end if

      if (stmt(1:3).eq.'set') then
        ipar=indpar(parse(line,2))
        if (ipar.ne.0) then
          call rdpar(line,3,parini(ipar,nfile),parvar(ipar,nfile),
     &               parmin(ipar,nfile),parmax(ipar,nfile))
        end if
      end if

      if (stmt(1:2).eq.'gr') then
        ipar=indpar(parse(line,2))
        if (ipar.ne.0) then
          if (partyp(ipar).eq.'g') then
            sg=parse(line,3)
            if (sg(1:1).eq.'c') then
              igc(ipar)=igc(ipar)-1
              igroup(ipar,nfile)=igc(ipar)
              parini(ipar,mfile+igc(ipar)+1)=val(parse(line,4))
            else
              if (sg(1:1).eq.'+') then
                lastgr=val(grlab(ipar,igroup(ipar,nfile-1)))
                write(ngrlra,'(i50)') lastgr+1
                do i=1,50
                  if (ngrlra(i:i).ne.' ') goto 202
                end do
                i=1
 202            nextgrl=ngrlra(i:50)
              else
                nextgrl=sg
              end if
              if (sg(1:1).ne.'=') then
                do i=1,ng(ipar)
                  if (nextgrl.eq.grlab(ipar,i)) then
                    igroup(ipar,nfile)=i
                    goto 201
                  end if
                end do
                ng(ipar)=ng(ipar)+1
                grlab(ipar,ng(ipar))=nextgrl
                igroup(ipar,nfile)=ng(ipar)
 201            continue
              else
                igroup(ipar,nfile)=igroup(ipar,nfile-1)
              end if
              if (parse(line,4).ne.' ') then
                call rdpar(line,4,
     &                     parini(ipar,igroup(ipar,nfile)),
     &                     parvar(ipar,igroup(ipar,nfile)),
     &                     parmin(ipar,igroup(ipar,nfile)),
     &                     parmax(ipar,igroup(ipar,nfile)))
              end if
            end if
          else
            call rdpar(line,4,parini(ipar,nfile),parvar(ipar,nfile),
     &                 parmin(ipar,nfile),parmax(ipar,nfile))
          end if
        end if
      end if

      if (stmt(1:3).eq.'wei') then
        iweight=1
      end if

      if (stmt(1:3).eq.'err') then
        ema2=val(parse(line,2))**2
        ems=val(parse(line,3))
        emr2=val(parse(line,4))**2
        if (ierrm.eq.0) then
          do ifile=1,nfile-1
            errab2(ifile)=ema2
            errsqt(ifile)=ems
            errrel(ifile)=emr2
          end do
          iweight=1
          ierrm=1
        end if
      end if
      if (ierrm.ne.0) then
        errab2(nfile)=ema2
        errsqt(nfile)=ems
        errrel(nfile)=emr2
      end if

      if (stmt(1:3).eq.'pri') then
        ipr0=val(parse(line,2))
      end if

      if ((stmt(1:4).eq.'maxf').and.(maxfun.ne.0)) then
        maxfun=val(parse(line,2))
      end if

      if (stmt(1:3).eq.'nof') then
        maxfun=0
      end if

      if (stmt(1:3).eq.'acc') then
        acc=val(parse(line,2))
      end if

      if (stmt(1:4).eq.'maxd') then
        dmax=val(parse(line,2))
      end if

      if (stmt(1:4).eq.'step') then
        dstep=val(parse(line,2))
      end if

      if (stmt(1:3).eq.'sho') then
        arg=parse(line,2)
C       write(*,*) "SHOW ",arg
        if (arg(1:3).eq.'dat') ishda=1
        if (arg(1:3).eq.'par') ishpa=1
        if (arg(1:4).eq.'kova') ikova=1
        if (arg(1:4).eq.'va05') iva05=1
      end if

      if (stmt(1:3).eq.'tra') then
        itrace=val(parse(line,2))
        if (itrace.eq.0) itrace=1
      end if

      if (stmt(1:3).eq.'dep') then
        arg=parse(line,2)
        if (arg(1:1).eq.'f') idepa=0
        if (arg(1:1).eq.'w') idepa=1
        if (arg(1:1).eq.'k') idepa=2
      end if

      if (stmt(1:4).eq.'cont') then
        pfile=parse1(line,2)
        if (pfile(1:1).eq.' ') pfile='unifit_para.txt'
        icont=1
      end if

      if (stmt(1:3).eq.'the') then
        itheo=1
        arg=parse(line,2)
        if ((arg(1:1).eq.'*').or.(arg(1:1).eq.' ')) then
          txmin=amagic
        else
          txmin=val(arg)
        end if
        arg=parse(line,3)
        if ((arg(1:1).eq.'*').or.(arg(1:1).eq.' ')) then
          txmax=amagic
        else
          txmax=val(arg)
        end if
        nintt=val(parse(line,4))
        if (nintt.eq.0) nintc=100
        if (nintt.gt.mint) nintt=mint
        arg=parse(line,5)
        if (arg(1:3).eq.'log') itlog=1
      end if

      if (stmt(1:4).eq.'calc') then
        icalc=1
        arg=parse(line,2)
        if ((arg(1:1).eq.'*').or.(arg(1:1).eq.' ')) then
          cxmin=amagic
        else
          cxmin=val(arg)
        end if
        arg=parse(line,3)
        if ((arg(1:1).eq.'*').or.(arg(1:1).eq.' ')) then
          cxmax=amagic
        else
          cxmax=val(arg)
        end if
        nintc=val(parse(line,4))
        if (nintc.eq.0) nintc=100
        if (nintc.gt.mint) nintc=mint
        arg=parse(line,5)
        if (arg(1:3).eq.'log') iclog=1
      end if

      if (stmt(1:4).eq.'plot') then
        iplot=1
        narg=2
 7      arg=parse(line,narg)
        if (arg(1:1).eq.' ') goto 8
C       write(*,*) narg,'|',arg,'|'
        if (arg(1:3).eq.'err') ierrpl=1
        if (arg(1:4).eq.'logx') iplogx=1
        if (arg(1:4).eq.'logy') iplogy=1
        if (arg(1:2).eq.'th') iplth=1
        if (arg(1:1).eq.'x') then
          narg=narg+1
          xpmin=val(parse(line,narg))
          narg=narg+1
          xpmax=val(parse(line,narg))
        end if
        if (arg(1:1).eq.'y') then
          narg=narg+1
          ypmin=val(parse(line,narg))
          narg=narg+1
          ypmax=val(parse(line,narg))
        end if
        if (arg(1:3).eq.'off') then
          narg=narg+1
          xoff=val(parse(line,narg))
          narg=narg+1
          yoff=val(parse(line,narg))
        end if
        narg=narg+1
        goto 7
 8      continue
      end if

      if (stmt(1:3).eq.'rpl') then
        irplot=1
        narg=2
 51     arg=parse(line,narg)
        if (arg(1:1).eq.' ') goto 52
        if (arg(1:4).eq.'logx') irlogx=1
        if (arg(1:1).eq.'x') then
          narg=narg+1
          xrpmin=val(parse(line,narg))
          narg=narg+1
          xrpmax=val(parse(line,narg))
        end if
        if (arg(1:1).eq.'y') then
          narg=narg+1
          yrpmin=val(parse(line,narg))
          narg=narg+1
          yrpmax=val(parse(line,narg))
        end if
        narg=narg+1
        goto 51
 52     continue
      end if

      if (stmt(1:3).eq.'out') then
        arg=parse(line,2)
        if (arg(1:3).eq.'dat') iodat=1
        if (arg(1:3).eq.'the') iothe=1
        if (arg(1:3).eq.'fit') iofit=1
        if (arg(1:3).eq.'res') iresi=1
        if (arg(1:3).eq.'fil') then
          narg=3
 17       arg=parse(line,narg)
C         write(*,*) narg,'|',arg,'|'
          if (arg(1:1).eq.' ') goto 18
          narg=narg+1
          ncolo=ncolo+1
          if (arg(1:5).eq.'$file') then
            icospc(ncolo)=-1
          else
            if (arg(1:5).eq.'#file') then
              icospc(ncolo)=-2
            else
              if (arg(1:4).eq.'$ssq') then
                icospc(ncolo)=-3
              else
                if (arg(1:5).eq.'$stat') then
                  icospc(ncolo)=-4
                else
                  if (arg(1:1).eq.'%') then
                    eparn=arg(2:50)
                    ipar=indpar(eparn)
                    if (ipar.gt.0) then
                      icospc(ncolo)=1000+ipar
                    else
                      ncolo=ncolo-1
                    end if
                  else
                    if (arg(1:3).eq.'err') then
                      eparn=arg(4:50)
                      ipar=indpar(eparn)
                      if (ipar.gt.0) then
                        icospc(ncolo)=1000+ipar
                      else
                        ncolo=ncolo-1
                      end if
                    else
                      ipar=indpar(arg)
                      if (ipar.gt.0) then
                        icospc(ncolo)=ipar
                      else
                        ncolo=ncolo-1
                      end if
                    end if
                  end if
                end if
              end if
            end if
          end if
          goto 17
        end if
 18     continue
      end if

      if (stmt(1:3).eq.'cfi') then
        if (iconv1.eq.0) iconv1=1
        iconv(max(nfile,1))=iconv1
        ilft=1
        ndr=ndr+1
        idr(max(nfile,1))=ndr
        rfnam(ndr)=parse1(line,2)
        icolrx(ndr)=1
        icolry(ndr)=2
        ibrowr(ndr)=1
        ierowr(ndr)=maxint
      arg=parse(line,3)
      if (arg(1:4).eq.'nono') inono(ndr)=1
      end if

      if (stmt(1:2).eq.'cm') then
        arg=parse(line,2)
        if (arg(1:1).eq.'p') iconv1=1
        if (arg(1:1).eq.'n') iconv1=2
        if (arg(1:11).eq.'numeric_old') iconv1=4
        if (arg(1:1).eq.'a') iconv1=3
        if (arg(1:1).eq.'-') iconv1=0
        if (nfile.ge.1) iconv(nfile)=iconv1
      end if

      if (stmt(1:4).eq.'boot') then
        nboot=val(parse(line,2))
        pboot=parse(line,3)
        if (pboot(1:1).eq.' ') then
          itboot=1
        else
          itboot=val(pboot)
        end if
        oboot=parse(line,4)
        if (oboot(1:4).eq.'diag') ibdiag=1
C       write(*,*) nboot,pboot,itboot,oboot,ibdiag
      end if

      if (stmt(1:4).eq.'mont') then
        imc=1
      end if

      if (stmt(1:4).eq.'moni') then
        imoni=1
      end if

      goto 5

C     read data files

 10   continue

      ndat=0
      nmdpf=0
      do ifile=1,nfile
C       write(*,*) icolx(ifile),icoly(ifile),icole(ifile)
        open(25,file=fnam(ifile),status='old',err=20)
        ndpf(ifile)=0
        do irow=1,ibrow(ifile)-1
          read(25,'(a)') dline
        end do
        idpf=0
        do irow=ibrow(ifile),ierow(ifile)
          read(25,'(a)',end=15) dline
          xd1=val1(parse(dline,icolx(ifile)))
          yd1=val1(parse(dline,icoly(ifile)))
          ed1=val(parse(dline,icole(ifile)))
C         write(*,*) xd1,amagic
          if (.not.((xd1.eq.amagic).or.(yd1.eq.amagic))) then
            idpf=idpf+1
            xd2(ifile,idpf)=xd1
            yd2(ifile,idpf)=yd1
            ed2(ifile,idpf)=ed1
            if ((errab2(ifile).ne.0.0).or.(errsqt(ifile).ne.0.0)
     &           .or.(errrel(ifile).ne.0.0)) then
              err2=errab2(ifile)+errsqt(ifile)*yd1+errrel(ifile)*yd1**2
              if (err2.le.0.0) then
                ed1=0.0
              else
                ed1=sqrt(err2)
              end if
            end if
            if (idpf.gt.ndpf(ifile)) ndpf(ifile)=idpf
            if (idpf.gt.nmdpf) nmdpf=idpf
            if (idpf.gt.mdat) call exit(111)
            if
     &       (((xmin(ifile).eq.amagic).or.(xd1.ge.xmin(ifile))).and.
     &       ((xmax(ifile).eq.amagic).or.(xd1.le.xmax(ifile))).and.
     &       ((ymin(ifile).eq.amagic).or.(yd1.ge.ymin(ifile))).and.
     &       ((ymax(ifile).eq.amagic).or.(yd1.le.ymax(ifile)))) then
              ndat=ndat+1
              if (ndat.gt.mdatt) call exit(110)
              xd(ndat)=xd1
              yd(ndat)=yd1
              ed(ndat)=ed1
              idf(ndat)=ifile
              if ((iweight.ne.0).and.(ed1.eq.0.0)) then
                iweight=0
                write(*,*)
     &           'Error value zero or imaginary: no weighting!'
              end if
            end if
          end if
        end do
 15     close(25)
      end do
      goto 30
 20   write(*,*) 'Data file ',fnam(ifile),'does not exist!'
      call exit(100)

 30   write(*,'(a,i0)') 'Data points read: ',ndat

      if (ishda.ne.0) then
        write(*,'(a)')
     &    '     x-value     y-value     y-error      file #'
        do idat=1,ndat
          write(*,'(3f12.4,i12)') xd(idat),yd(idat),ed(idat),
     &                            idf(idat)
        end do
      end if

C     read convolution files

      isconv=iconv(1)
      do ifile=2,nfile
        if (isconv.ne.iconv(ifile)) then
          isconv=-1
          goto 33
        end if
      end do

 33   if (ifile.eq.nfile) write(*,'(a)')
     &  "Warning: iconv changed only for last file (not globally)!"

      if (isconv.eq.0) then
        write(*,'(a)') "No convolution"
      else
        if (isconv.gt.0) then
          write(*,'(a,i0)') 'Convolution type ',isconv
        else
          do ifile=1,nfile
            if (iconv(ifile).eq.0) then
              write(*,'(a,i0,a)') "File ",ifile,": no convolution"
            else
              write(*,'(a,i0,a,i0)')
     &  "File ",ifile,": convolution type ",iconv(ifile)
            end if
          end do
        end if
      end if

      if (isconv.ne.0) then
        do idr1=1,ndr
          open(25,file=rfnam(idr1),status='old',err=25)
          ir=0
          do irow=1,ibrowr(idr1)-1
            read(25,'(a)') dline
          end do
          do irow=ibrowr(idr1),ierowr(idr1)
            read(25,'(a)',end=16) dline
            xr1=val(parse(dline,icolrx(idr1)))
            yr1=val(parse(dline,icolry(idr1)))
            if (((xrmin(idr1).eq.amagic).or.(xr1.ge.xrmin(idr1))).and.
     &          ((xrmax(idr1).eq.amagic).or.(xr1.le.xrmax(idr1))).and.
     &          ((yrmin(idr1).eq.amagic).or.(yr1.ge.yrmin(idr1))).and.
     &          ((yrmax(idr1).eq.amagic).or.(yr1.le.yrmax(idr1)))) then
              ir=ir+1
              xru(ir)=xr1
              yru(ir)=yr1
            end if
          end do
 16       close(25)
          if (ir.gt.mres) call exit(115)
          nres(idr1)=ir
          call kb07ad(xru,nres(idr1),indr)
          do ir=1,nres(idr1)
            xr(ir,idr1)=xru(ir)
            yr(ir,idr1)=yru(indr(ir))
          end do
        end do
        goto 35
 25     write(*,*) 'Resolution file ',rfnam(idr1),' does not exist!'
        call exit(100)
      end if

C     continue with saved parameter set if desired:

 35   if (icont.eq.1) then
        open(45,file=pfile,status='old')
 71     read(45,'(a)',end=72) pline
        if (pline(1:11).eq.'Parameters:') goto 73
        goto 71
 72     rewind(45)
 73     read(45,'(a)',end=74) pline
        pnam=parse(pline,1)
        if (pnam(1:3).eq.'end') goto 74
        idfstr=parse(pline,2)
C      write(*,*) idfstr
        ipar=indpar(pnam)
        if (ipar.gt.0) then
          if (idfstr(1:1).eq.'*') then
            do ifile=1,nfile
              parini(ipar,ifile)=val(parse(pline,3))
            end do
          else
          if ((idfstr(2:2).eq.':').or.(idfstr(3:3).eq.':')
     &          .or.(idfstr(4:4).eq.':')) then
              parini(ipar,int(val(idfstr)))=val(parse(pline,4))
          else
              parini(ipar,int(val(idfstr)))=val(parse(pline,3))
          end if
          end if
        end if
        goto 73
      end if
 74   close(45)

      write(*,'(a,i0)') 'Physical fit parameters: ',npar

      nptf=0
      do ipar=1,npar
C       write(*,*) ipar,partyp(ipar)
        if (partyp(ipar).eq.'f') then
        call cgen(parini(ipar,1),parvar(ipar,1),parmin(ipar,1),
     &              parmax(ipar,1),isca(ipar,1),asca(1,ipar,1))
          nptf=nptf+1
          if (ishpa.ne.0)
     &      write(*,'(i3,": ",a20,a2,4e12.4)')
     &        nptf,parnam(ipar),partyp(ipar),parini(ipar,1),
     &        parvar(ipar,1),parmin(ipar,1),parmax(ipar,1)
        end if
        if (partyp(ipar).eq.'i') then
          nptf=nptf+nfile
          do ifile=1,nfile
          call cgen(parini(ipar,ifile),parvar(ipar,ifile),
     &                parmin(ipar,ifile),parmax(ipar,ifile),
     &                isca(ipar,ifile),asca(1,ipar,ifile))
            if (ishpa.ne.0)
     &        write(*,'(i3,": ",a20,a2,4e12.4)')
     &          nptf-nfile+ifile,parnam(ipar),partyp(ipar),
     &          parini(ipar,ifile),parvar(ipar,ifile),
     &          parmin(ipar,ifile),parmax(ipar,ifile)
          end do
        end if
        if (partyp(ipar).eq.'g') then
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
          if (ig.gt.0) then
            call cgen(parini(ipar,ig),parvar(ipar,ig),
     &                  parmin(ipar,ig),parmax(ipar,ig),
     &                  isca(ipar,ig),asca(1,ipar,ig))
              if (ishpa.ne.0)
     &          write(*,'(i3,": ",a20,a2,4e12.4)')
     &            nptf+ig,parnam(ipar),partyp(ipar),
     &            parini(ipar,ig),parvar(ipar,ig),
     &            parmin(ipar,ig),parmax(ipar,ig)
              if (ig.gt.ng(ipar)) ng(ipar)=ig
          end if
          end do
          nptf=nptf+ng(ipar)
        end if
C       write(*,*) ipar,nptf
      end do

      write(*,'(a,i0)') 'Parameters-to-fit: ',nptf

C     do the fit:

      call fn_ini

      do iptf=1,nptf
        ptf(iptf)=0.0
      end do
      if (iweight.ne.0) then
        do idat=1,ndat
          swd(idat)=1.0/ed(idat)
        end do
      end if

C     write(*,*) dstep,dmax,acc,maxfun,iprint
      nfun=0
      if (maxfun.le.0) then
        call calfun(ndat,nptf,f,ptf)
        do ipar=1,npar
          do ifile=1,nfile
C           respar(ipar,ifile)=parini(ipar,ifile)
            call ptf2par(ptf,npar,nfile,respar)
            errpar(ipar,ifile)=0.0d0
          end do
        end do
        goto 43
      end if
      if (itrace.ge.2) then
        do ipar=1,npar
          write(*,'(2x,a12,$)') parnam(ipar)
        end do
        if (itrace.ge.4) then
          write(*,'(a)') '  x             y'
        else
          write(*,*)
        end if
      end if
      if (itrace.eq.0) then
        write(*,'(a,$)') 'Fit function calls:     0'
      end if
      if (imc.eq.0) then
        iprint=ipr0
        if (iva05.ne.0) iprint=1
        call va05ad(ndat,nptf,f,ptf,dstep,dmax,acc,maxfun,iprint,w)
        iprf=iprint
        write(*,*)
        if (ipr0.eq.0) then
          if (iprint.eq.1)
     &      write(*,'(a)') 'VA05AD: Sum of squares fails to decrease!'
          if (iprint.eq.2)
     &      write(*,'(a)') 'VA05AD: Maximal number of function calls!'
          if (iprint.gt.2)
     &      write(*,'(a,i4)') 'Unknown VA05AD error:',iprint
        end if
      else
        call mcfit(ndat,nptf,f,ptf,maxfun)
      end if
      call ptf2par(ptf,npar,nfile,respar)

C     do the error calculation by KOVA:

      if (imc.eq.0) then
        do idat=1,ndat
          if (iweight.ne.0) then
            wd(idat)=ed(idat)**(-2)
          else
            wd(idat)=1.0
          end if
        end do
        nfun1=nfun
        nfun=-1
 81     if (ikova.ne.0) then
          open(20,file="/dev/tty")
        else
          open(20,file='unifit_kova.txt',status='unknown')
        end if
        call kova(xd,yd,wd,ptf,dptf,dstep,ndat,nptf,ierr)
        close(20)
        if (ierr.ne.0) then
          if (idepa.eq.0) call exit(200)
          if ((idepa.eq.2).and.(ikova.eq.0)) then
            ikova=1
            goto 81
          end if
        else
          call dptf2dp(ptf,dptf,npar,nfile,errpar)
        end if
      end if
      ssq=0.0
      do idat=1,ndat
        ssq=ssq+f(idat)**2
      end do

C     do the error calculation by bootstrap (if requested):

      if (itboot.eq.0) goto 108

C     Write parameters of 'classic' fits to file for bootana:

      open(51,file='unifit_noboot.txt',status='unknown')
      do ipar=1,npar
        if (partyp(ipar).eq.'f') then
          write(51,110) parnam(ipar),respar(ipar,1),errpar(ipar,1)
        end if
        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
            write(51,111)
     &        parnam(ipar),ifile,respar(ipar,ifile),errpar(ipar,ifile)
          end do
        end if
        if (partyp(ipar).eq.'g') then
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
            if (ig.gt.0) then
              write(51,114)
     &          parnam(ipar),grlab(ipar,ig),ifile,
     &            respar(ipar,ifile),errpar(ipar,ifile)
              else
                write(51,113)
     &            parnam(ipar),ifile,respar(ipar,ifile)
            end if
          end do
        end if
      end do
      close(51)

C     Save old (reference) state of fit parameters, data and function:
      itrace0=itrace
      itrace=0
      iweight0=iweight
      do iptf=1,nptf
        ptf0(iptf)=ptf(iptf)
      end do
      do idat=1,ndat
        yd0(idat)=yd(idat)
        fct0(idat)=fct(idat)
      end do

C     Start boot parameter file: 1. names, 2. file numbers, 3. reference
      open(55,file='unifit_boot.txt',status='unknown')
      call wrfl(1,55,npar,nfile,respar)
      call wrfl(2,55,npar,nfile,respar)
      call wrfl(3,55,npar,nfile,respar)

      do ipar=1,npar
        do ifile=1,nfile
          dev2(ipar,ifile)=0.0
        end do
      end do

      if (itboot.eq.1) then

C       Bayesian bootstrap:

C       loop over boot replicas:
        do iboot=1,nboot

          if (ibdiag.ne.0) then
            write(bdname,"('boot_diag_',i4.4)") iboot
            open(56,file=bdname,status='unknown')
          end if

C         generate the weight modifiers:
          do idat=1,ndat-1
            pw(idat)=rand(0)
          end do
          call kb07ad(pw,ndat-1,indx)
          fw(1)=(pw(1))*ndat
          do idat=2,ndat-1
            fw(idat)=(pw(idat)-pw(idat-1))*ndat
          end do
          fw(ndat)=(1.0-pw(ndat-1))*ndat
C         do i=1,ndat
C           write(*,*) pw(i),fw(i)
C         end do

C         calculate modified square-roots of weights:
          if (iweight0.ne.0) then
            do idat=1,ndat
              swd(idat)=sqrt(fw(idat))/ed(idat)
            end do
          else
            do idat=1,ndat
              swd(idat)=sqrt(fw(idat))
            end do
          end if

C         Fit start values, three possibilities:
C         1. restart at scaled 0
          do iptf=1,nptf
            ptf(iptf)=0.0
          end do
C         2. start with canonical fit
C         do iptf=1,nptf
C           ptf(iptf)=ptf0(iptf)
C         end do
C         3. start with last fit

C         do the replica fit:
          iweight=1
          iprint=0
          nfun=0
          call va05ad(ndat,nptf,f,ptf,dstep,dmax,acc,maxfun,iprint,w)
          write(*,'(" Boot replica #",i3," error code",i2)') iboot,iprint

          if (ibdiag.ne.0) then
            do idat=1,ndat
              write(56,'(4e13.5)') xd(idat),yd(idat),swd(idat),fct(idat)
            end do
          end if

C         calculate the individual parameters and deviation:
          call ptf2par(ptf,npar,nfile,bpar)
          call wrfl(3,55,npar,nfile,bpar)
          do ipar=1,npar
            do ifile=1,nfile
              dev2(ipar,ifile)=dev2(ipar,ifile)+
     &          (bpar(ipar,ifile)-respar(ipar,ifile))**2
            end do
          end do

          if (ibdiag.ne.0) close(56)

        end do

      end if

      if ((itboot.eq.2).or.(itboot.eq.3)) then

C       Classic Monte Carlo error estimation (3: from data)

C       loop over replicas:
        do iboot=1,nboot

          if (ibdiag.ne.0) then
            write(bdname,"('boot_diag_',i4.4)") iboot
            open(56,file=bdname,status='unknown')
          end if

C         generate 'alternative facts':
          if (itboot.eq.2) then
            do idat=1,ndat
              yd(idat)=fct0(idat)+ed(idat)*grand(0)
            end do
          else
            do idat=1,ndat
              yd(idat)=yd0(idat)+ed(idat)*grand(0)
            end do
          end if
C         write(*,*) yd0(11),yd(11),ed(11)

C         Fit start values, three possibilities:
C         1. restart at scaled 0
          do iptf=1,nptf
            ptf(iptf)=0.0
          end do
C         2. start with canonical fit
C         do iptf=1,nptf
C           ptf(iptf)=ptf0(iptf)
C         end do
C         3. start with last fit

C         do the replica fit:
          iweight=1
          iprint=0
          nfun=0
          call va05ad(ndat,nptf,f,ptf,dstep,dmax,acc,maxfun,iprint,w)
          write(*,'(" MC replica #",i3," error code",i2)') iboot,iprint

          if (ibdiag.ne.0) then
            do idat=1,ndat
              write(56,'(4e13.5)') xd(idat),yd(idat),swd(idat),fct(idat)
            end do
          end if

C         calculate the individual parameters and deviation:
          call ptf2par(ptf,npar,nfile,bpar)
          call wrfl(3,55,npar,nfile,bpar)
          do ipar=1,npar
            do ifile=1,nfile
              dev2(ipar,ifile)=dev2(ipar,ifile)+
     &          (bpar(ipar,ifile)-respar(ipar,ifile))**2
            end do
          end do

          if (ibdiag.ne.0) close(56)

        end do

      end if

      do ipar=1,npar
        do ifile=1,nfile
          errpar(ipar,ifile)=sqrt(dev2(ipar,ifile)/nboot)
        end do
      end do

      close(55)

C     restore previous state:
      itrace=itrace0
      iweight=iweight0
      do iptf=1,nptf
        ptf(iptf)=ptf0(iptf)
      end do
      do idat=1,ndat
        swd(idat)=1.0/ed(idat)
      end do
      do idat=1,ndat
        yd(idat)=yd0(idat)
        fct(idat)=fct0(idat)
      end do
C     call calfun(ndat,nptf,f,ptf)

C     show the fit results:

 108  continue

 43   if (iresi.ne.0) then
        open(45,file='unifit_res.txt',status='unknown')
        write(45,'(a)')
     &    "# file            x            y         f(x)          res"
      do idat=1,ndat
        write(45,151) idf(idat),xd(idat),yd(idat),fct(idat),f(idat)
 151      format("  ",i4,4e13.5)
      end do
        close(45)
      end if
      if (iweight.eq.0) then
        write(*,'(a,e10.4)') 'Sum of squares:        ',ssq
      else
        if ((ssq.lt.1.0e6).and.(ssq.gt.0.1)) then
          write(*,101) ssq,ndat-nptf
 101      format('Chi-squared:  ',f11.4,' (',i5,')')
        else
          write(*,102) ssq,ndat-nptf
 102      format('Chi-squared:  ',e11.4,' (',i5,')')
        endif
      end if

      write(*,'(a)')
     &  'Parameter          [Grp:]    File#       Value  Error est.'
      do ipar=1,npar
        if (partyp(ipar).eq.'f') then
          write(*,110) parnam(ipar),respar(ipar,1),errpar(ipar,1)
 110      format(a20,'             *',2e12.4)
        end if
        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
            write(*,111)
     &        parnam(ipar),ifile,respar(ipar,ifile),errpar(ipar,ifile)
 111        format(a20,'        ',i6,2e12.4)
          end do
        end if
        if (partyp(ipar).eq.'g') then
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
            if (ig.gt.0) then
              write(*,114)
     &          parnam(ipar),grlab(ipar,ig),ifile,
     &          respar(ipar,ifile),errpar(ipar,ifile)
 114          format(a20,a10,': ',i2,2e12.4)
            else
              write(*,113)
     &          parnam(ipar),ifile,respar(ipar,ifile)
            end if
          end do
        end if
        if (partyp(ipar).eq.'c') then
          write(*,112) parnam(ipar),respar(ipar,1)
 112      format(a20,'             *',e12.4,'     const')
        end if
        if (partyp(ipar).eq.'p') then
          do ifile=1,nfile
            write(*,113)
     &        parnam(ipar),ifile,respar(ipar,ifile)
 113        format(a20,'        ',i6,e12.4,'     const')
          end do
        end if
      end do

C     write the results:

      open(41,file='unifit_para.txt',status='unknown')
      write(41,'(2a/)') 'Control file: ',cfname
      write(41,'(a)') 'Files:'
      do ifile=1,nfile
        write(41,'(i2,2a)') ifile,': ',fnam(ifile)
      end do
      write(41,*)
      if (ncode.gt.0) then
        write(41,'(a)') 'Inline function:'
        do icode=1,ncode
          write(41,'(a)') code(icode)
        end do
      end if
      if (iexff.gt.0) then
        write(41,'(a,a)') 'External FORTRAN function: ',ffile
      end if
      if (nexsf.gt.0) then
        do iexsf=1,nexsf
          write(41,'(a,a)') 'External subroutine: ',sfile(iexsf)
        end do
      end if
      write(41,*)
      if (itboot.gt.0) then
        write(41,'("Bootstrap method",i2,", replicas:",i4)')
     &    itboot,nboot
      end if
      write(41,*)
      write(41,'(a,i6)') 'Fit function calls:       ',nfun1
      if (ipr0.eq.0) then
        if (iprint.eq.1)
     &    write(41,'(a)') 'VA05AD: Sum of squares fails to decrease!'
        if (iprint.eq.2)
     &    write(41,'(a)') 'VA05AD: Maximal number of function calls!'
        if (iprint.gt.2)
     &    write(41,'(a,i4)') 'Unknown VA05AD error:',iprint
      end if
      if (iweight.eq.0) then
        write(41,'(a,e10.4/)') 'Sum of squares:        ',ssq
      else
        write(41,"('Chi-squared:  ',f11.4,' (',i5,')'/)") ssq,ndat-nptf
      end if
      write(41,'(a)') 'Parameters:'
      do ipar=1,npar
        if (partyp(ipar).eq.'f') then
          write(41,110) parnam(ipar),respar(ipar,1),errpar(ipar,1)
        end if
        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
            write(41,111)
     &        parnam(ipar),ifile,respar(ipar,ifile),errpar(ipar,ifile)
          end do
        end if
        if (partyp(ipar).eq.'g') then
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
            if (ig.gt.0) then
              write(41,114)
     &          parnam(ipar),grlab(ipar,ig),ifile,
     &            respar(ipar,ifile),errpar(ipar,ifile)
              else
                write(41,113)
     &            parnam(ipar),ifile,respar(ipar,ifile)
            end if
          end do
        end if
        if (partyp(ipar).eq.'c') then
          write(41,112) parnam(ipar),respar(ipar,1)
        end if
        if (partyp(ipar).eq.'p') then
          do ifile=1,nfile
            write(41,113)
     &        parnam(ipar),ifile,respar(ipar,ifile)
          end do
        end if
      end do
      close(41)

      if (ncolo.ne.0) then
        open(42,file='unifit_parf.txt',status='unknown')
        do ifile=1,nfile
          do icol=1,ncolo
            if (icospc(icol).eq.-1) then
              fn=fnam(ifile)
              fnc="                               "
              j=31
              do i=50,1,-1
                ch=fn(i:i)
                if (ch.eq." ") goto 157
                if (ch.eq."/") goto 158
                fnc(j:j)=ch
                j=j-1
                if (j.eq.1) goto 158
 157          end do
 158          write(42,'(a,$)') fnc
            end if
            if (icospc(icol).eq.-2) write(42,'(i4,$)') ifile
            if (icospc(icol).eq.-3) write(42,'(e12.4,$)') ssq
            if (icospc(icol).eq.-4) write(42,'(i4,$)') iprf
            if (icospc(icol).gt.1000) then
              write(42,'(e12.4,$)') errpar(icospc(icol)-1000,ifile)
            else
              if (icospc(icol).gt.0) then
                write(42,'(e12.4,$)') respar(icospc(icol),ifile)
              end if
            end if
          end do
          write(42,*)
        end do
        close(42)
      end if

      if (iodat.eq.1) then
        open(45,file='unifit_dat.txt',status='unknown')
        do idpf=1,nmdpf
          if (idpf.le.ndpf(1)) then
            write(45,133) xd2(1,idpf),yd2(1,idpf),ed2(1,idpf)
 133        format(e12.5,',',e12.5,',',e12.5,$)
          else
            write(45,'(a5,$)') '-,-,-'
          end if
          do ifile=2,nfile
            if (idpf.le.ndpf(ifile)) then
              write(45,134)
     &          xd2(ifile,idpf),yd2(ifile,idpf),ed2(ifile,idpf)
 134          format(',',e12.5,',',e12.5,',',e12.5,$)
            else
              write(45,'(a6,$)') ',-,-,-'
            end if
          end do
          write(45,*)
       end do
       close(45)
      end if

C     calculate the fit function:

      if (icalc.eq.0) call exit(7)

      if (cxmax.eq.amagic) then
        cxmax=xd(1)
        do idat=1,ndat
          if (cxmax.lt.xd(idat)) cxmax=xd(idat)
        end do
      end if
      if (cxmin.eq.amagic) then
        cxmin=xd(1)
        do idat=1,ndat
          if (cxmin.gt.xd(idat)) cxmin=xd(idat)
        end do
      end if

      idf1=0
      npar1=npar
      newpar=1
      do ifile=1,nfile
        write(dfname,"('unifit_fitf_',i2.2)") ifile
        open(32,file=dfname,status='unknown')
        idf1=ifile
        if (ipgi.ne.0) newpar=1
        if (iclog.eq.0) then
          dx=(cxmax-cxmin)/nintc
          do i=0,nintc
            xc(ifile,i+1)=cxmin+i*dx
            do ipar=1,npar
              par1(ipar)=respar(ipar,ifile)
            end do
            yc(ifile,i+1)=convfun(xc(ifile,i+1))
            write(32,*) xc(ifile,i+1),yc(ifile,i+1)
          end do
        else
          dlgx=(log10(cxmax)-log10(cxmin))/nintc
          do i=0,nintc
            xc(ifile,i+1)=cxmin*10.**(i*dlgx)
            do ipar=1,npar
              par1(ipar)=respar(ipar,ifile)
            end do
            yc(ifile,i+1)=convfun(xc(ifile,i+1))
            write(32,*) xc(ifile,i+1),yc(ifile,i+1)
          end do
        end if
        close(32)
      end do

      if (iofit.eq.1) then
        open(44,file='unifit_fit.txt',status='unknown')
        do i=1,nintc+1
          write(44,131) xc(1,i),yc(1,i)
 131      format(e12.5,',',e12.5,$)
          do ifile=2,nfile
            write(44,132) xc(ifile,i),yc(ifile,i)
 132        format(',',e12.5,',',e12.5,$)
          end do
          write(44,*)
        end do
        close(44)
      end if

C     calculate the theory function:

      if (itheo.eq.0) goto 150

      if (txmax.eq.amagic) then
        txmax=xd(1)
        do idat=1,ndat
          if (txmax.lt.xd(idat)) txmax=xd(idat)
        end do
      end if
      if (txmin.eq.amagic) then
        txmin=xd(1)
        do idat=1,ndat
          if (txmin.gt.xd(idat)) txmin=xd(idat)
        end do
      end if

      idf1=0
      npar1=npar
      do ifile=1,nfile
        write(dfname,"('unifit_thef_',i2.2)") ifile
        open(32,file=dfname,status='unknown')
        do ipar=1,npar
          par1(ipar)=respar(ipar,ifile)
        end do

C If the convolution file is not normalised for comparison with the
C data it is more reasonable to multiply the theory by the integral
C over the convolution data:
        idr1=idr(ifile)
        if (inono(idr1).ne.0) then
          sr=0.5*(xr(2,idr1)-xr(1,idr1))*yr(1,idr1)
          do j=2,nres(idr1)-1
            sr=sr+0.5*(xr(j+1,idr1)-xr(j-1,idr1))*yr(j,idr1)
          end do
          sr=sr+0.5*(xr(nres(idr1),idr1)-xr(nres(idr1)-1,idr1))
     &       *yr(nres(idr1),idr1)
          fnorm=sr
        else
          fnorm=1.0
        end if

        if (itlog.eq.0) then
          dx=(txmax-txmin)/nintt
          do i=0,nintt
            xt(ifile,i+1)=txmin+i*dx
            yt(ifile,i+1)=fnorm*fitfun(xt(ifile,i+1))
            write(32,*) xt(ifile,i+1),yt(ifile,i+1)
          end do
        else
          dlgx=(log10(txmax)-log10(txmin))/nintt
          do i=0,nintt
            xt(ifile,i+1)=txmin*10.**(i*dlgx)
            yt(ifile,i+1)=fnorm*fitfun(xt(ifile,i+1))
            write(32,*) xt(ifile,i+1),yt(ifile,i+1)
          end do
        end if
        close(32)
      end do

      if (iothe.eq.1) then
        open(44,file='unifit_the.txt',status='unknown')
        do i=1,nintt+1
          write(44,131) xt(1,i),yt(1,i)
          do ifile=2,nfile
            write(44,132) xt(ifile,i),yt(ifile,i)
          end do
          write(44,*)
        end do
        close(44)
      end if

C     Plot: first rewrite data to ensure identity with data
C     used in the fit

      iexc=0

 150  if (iplot.eq.0) then

        iexc=iexc+2
      
      else

        ifile=0
        do idat=1,ndat
          if (idf(idat).gt.ifile) then
            ifile=idf(idat)
            write(dfname,"('unifit_data_',i2.2)") ifile
            open(30,file=dfname,status='unknown')
          end if
          write(30,*) xd(idat),yd(idat),ed(idat)
          if (idf(idat+1).gt.ifile) close(30)
        end do

C       construct the gnuplot command file and start it:

        open(31,file='unifit_gnuplot.input',status='unknown')
        write(31,*) "set termoption noenhanced"
        if (cfname(1:12).ne.'unifit.input') then
          lcfn=length(cfname)
          write(31,'(3a)') "set key title '",cfname(1:lcfn),"'"
        end if
        write(31,*) "set key reverse"
        do igcom=1,ngcom
          write(31,'(a)') gcom(igcom)
        end do
        if (iplogx.eq.1)
     &   write(31,'(a)') 'set logscale x;set format x "1E%L"'
        if (iplogy.eq.1)
     &   write(31,'(a)') 'set logscale y;set format y "1E%L"'
        write(31,'(2a)') 'plot ',bsl
        if (xpmin.ne.xpmax) then
          write(31,121) xpmin,xpmax,bsl
 121      format('[',e12.4,':',e12.4,']',a)
        else
          write(31,'(2a)') '[ ] ',bsl
        end if
        if (ypmin.ne.ypmax) write(31,121) ypmin,ypmax,bsl
        maxfnl=0
        do ifile=1,nfile
          lfn=length(fnam(ifile))
          if (maxfnl.lt.lfn) maxfnl=lfn
        end do
        do ifile=nfile,1,-1
          if (iplogx.eq.0) then
            if (iplogy.eq.0) then
C             xlin/ylin
              xshift=xoff*(ifile-1)
              yshift=yoff*(ifile-1)
              write(dfname,"('unifit_data_',i2.2)") ifile
              if (ierrpl.ne.0) then
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i0,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"+",
     &          xshift,"):($",icoly(ifile),"+",yshift,
     &          "):",icole(ifile),"  notitle with errorbars lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1+",xshift,"):($2+",yshift,"):3 ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with errorbars lt ",ifile,",",bsl
              else
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"+",
     &          xshift,"):($",icoly(ifile),"+",yshift,
     &          ")  notitle with points lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1+",xshift,"):($2+",yshift,") ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with points lt ",ifile,",",bsl
              end if
              if ((iplth.ne.0).and.(itheo.ne.0)) then
                write(dfname,"('unifit_thef_',i2.2)") ifile
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile," lw 2,",bsl
              end if
              write(dfname,"('unifit_fitf_',i2.2)") ifile
              if (ifile.gt.1) then
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile,",",bsl
              else
                write(31,'(3a,e10.3,a,e10.3,2a,i2,a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile
              end if
            else
C             xlin/ylog
              xshift=xoff*(ifile-1)
              if (yoff.gt.0.0) then
                yshift=yoff**(ifile-1)
              else
                yshift=1.0
              end if
              write(dfname,"('unifit_data_',i2.2)") ifile
              if (ierrpl.ne.0) then
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i0,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"+",
     &          xshift,"):($",icoly(ifile),"*",yshift,
     &          "):",icole(ifile),"  notitle with errorbars lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1+",xshift,"):($2*",yshift,
C    &          "):($2*exp(-$3/$2)):($2*exp($3/$2)) ",
     &          "):3 ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with errorbars lt ",ifile,",",bsl
              else
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"+",
     &          xshift,"):($",icoly(ifile),"*",yshift,
     &          ")  notitle with points lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1+",xshift,"):($2*",yshift,") ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with points lt ",ifile,",",bsl
              end if
              if ((iplth.ne.0).and.(itheo.ne.0)) then
                write(dfname,"('unifit_thef_',i2.2)") ifile
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile," lw 2,",bsl
              end if
              write(dfname,"('unifit_fitf_',i2.2)") ifile
              if (ifile.gt.1) then
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile,",",bsl
              else
                write(31,'(3a,e10.3,a,e10.3,2a,i2,a)') "'",dfname,
     &          "' using ($1+",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile
              end if
            end if
          else
            if (iplogy.eq.0) then
C             xlog/ylin
              if (xoff.gt.0.0) then
                xshift=xoff**(ifile-1)
              else
                xshift=1.0
              end if
              yshift=yoff*(ifile-1)
              write(dfname,"('unifit_data_',i2.2)") ifile
              if (ierrpl.ne.0) then
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i0,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"*",
     &          xshift,"):($",icoly(ifile),"+",yshift,
     &          "):",icole(ifile),"  notitle with errorbars lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1*",xshift,"):($2+",yshift,"):3 ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with errorbars lt ",ifile,",",bsl
              else
               write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"*",
     &          xshift,"):($",icoly(ifile),"+",yshift,
     &          ")  notitle with points lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1*",xshift,"):($2+",yshift,") ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with points lt ",ifile,",",bsl
              end if
              if ((iplth.ne.0).and.(itheo.ne.0)) then
                write(dfname,"('unifit_thef_',i2.2)") ifile
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile," lw 2,",bsl
              end if
              write(dfname,"('unifit_fitf_',i2.2)") ifile
              if (ifile.gt.1) then
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile,",",bsl
              else
                write(31,'(3a,e10.3,a,e10.3,2a,i2,a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2+",yshift,") ",
     &          " notitle with lines lt ",ifile
              end if
            else
C             ylog/ylin
              if (xoff.gt.0.0) then
                xshift=xoff**(ifile-1)
              else
                xshift=1.0
              end if
              if (yoff.gt.0.0) then
                yshift=yoff**(ifile-1)
              else
                yshift=1.0
              end if
              write(dfname,"('unifit_data_',i2.2)") ifile
              if (ierrpl.ne.0) then
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i0,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"*",
     &          xshift,"):($",icoly(ifile),"*",yshift,
     &          "):",icole(ifile),"  notitle with errorbars lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1*",xshift,"):($2*",yshift,
     &          "):3 ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with errorbars lt ",ifile,",",bsl
              else
                write(31,'(3a,i0,a,e10.3,a,i0,a,e10.3,a,i2,2a)')
     &          "'",trim(fnam(ifile)),"' using ($",icolx(ifile),"*",
     &          xshift,"):($",icoly(ifile),"*",yshift,
     &          ")  notitle with points lt ",ifile,
     &          " lc rgb 'grey',",bsl
                write(31,'(3a,e10.3,a,e10.3,2a,i2,3a,i2,2a)') "'",
     &          dfname,"' using ($1*",xshift,"):($2*",yshift,") ",
     &          " title '",ifile,":",fnam(ifile)(1:maxfnl),
     &          "' with points lt ",ifile,",",bsl
              end if
              if ((iplth.ne.0).and.(itheo.ne.0)) then
                write(dfname,"('unifit_thef_',i2.2)") ifile
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile," lw 2,",bsl
              end if
              write(dfname,"('unifit_fitf_',i2.2)") ifile
              if (ifile.gt.1) then
                write(31,'(3a,e10.3,a,e10.3,2a,i2,2a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile,",",bsl
              else
                write(31,'(3a,e10.3,a,e10.3,2a,i2,a)') "'",dfname,
     &          "' using ($1*",xshift,"):($2*",yshift,") ",
     &          " notitle with lines lt ",ifile
              end if
          end if
        end if
        end do
        write(31,*) "set terminal pdf color solid noenhanced size 10,7"
        write(31,*) "set output 'unifit_plot.pdf"
        write(31,*) "replot"
        close(31)
      
      end if

C     plot the residuals if desired

      if (irplot.eq.0) then

        iexc=iexc+1

      else

        call calfun(ndat,nptf,f,ptf)
        ifile=0
        do idat=1,ndat
          if (idf(idat).gt.ifile) then
            ifile=idf(idat)
            write(rfname,"('unifit_res_',i2.2)") ifile
            open(35,file=rfname,status='unknown')
          end if
          write(35,*) xd(idat),f(idat)
          if (idf(idat+1).gt.ifile) close(35)
        end do

        open(32,file='unifit_resplot.input',status='unknown')
        write(32,*) "set termoption noenhanced"
        write(32,*) "set key reverse"
        if (irlogx.eq.1)
     &    write(32,'(a)') 'set logscale x;set format x "1E%L"'
        write(32,'(2a)') 'plot ',bsl
        if (xrpmin.ne.xrpmax) then
          write(32,121) xrpmin,xrpmax,bsl
        else
          write(32,'(2a)') '[ ] ',bsl
        end if
        if (yrpmin.ne.yrpmax) write(32,121) yrpmin,yrpmax,bsl
        do ifile=nfile,1,-1
          write(rfname,"('unifit_res_',i2.2)") ifile
          if (ifile.gt.1) then
            write(32,'(3a,i2,3a,i2,2a)') "'",rfname,
     &      "' title '",ifile,":",fnam(ifile)(1:maxfnl),
     &      "' with points lt ",ifile,",",bsl
          else
            write(32,'(3a,i2,3a,i2)') "'",rfname,
     &      "' title '",ifile,":",fnam(ifile)(1:maxfnl),
     &      "' with points lt ",ifile
          end if
        end do
        write(32,*) "set terminal pdf color solid noenhanced size 10,7"
        write(32,*) "set output 'unifit_resplot.pdf"
        write(32,*) "replot"
        close(32)

      end if

 99   call exit(iexc)

      end


      subroutine calfun(ndat,nptf,f,ptf)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension par(mpar),para(mpar,mfile),ptf(mptf)
      dimension xd(mdatt),yd(mdatt),ed(mdatt),swd(mdatt),idf(mdatt)
      dimension xr(mres,mfile),yr(mres,mfile),idr(mfile),nres(mfile)
      dimension iconv(mfile),inono(mfile)
      dimension d(mdel),w(mdel)
      dimension f(mdatt),fct(mdatt)
      character BS
C     parameter(BS=char(8))
      external convfun

C     this common corresponds to FITFUN:
      common/fitpar/ npar,idf1,newpar,idel,d,w,aconv,bconv,par
!$OMP THREADPRIVATE(/fitpar/)

C     these commons correspond to the main program:
      common/dat/ xd,yd,ed,swd,idf,iweight,npar1,nfile,nfun,
     &  itrace,ipgi,imoni
      common/conv/ xr,yr,nres,iconv,idr,inono,mflag

C     this common corresponds to KOVA:
      common/ff/ fct

C     write(*,*) ndat,nptf,f(1),ptf(1)

C     gimmick: if nfun>=0 it is counted up to show the increasing
C              number of calls of CALFUN
      if (nfun.ge.0) nfun=nfun+1
      if ((nfun.ge.0).and.(itrace.eq.0)) then
        BS=char(8)
C       nfun=nfun+1
        write(*,'(5a1,i5,$)') BS,BS,BS,BS,BS,nfun
      end if

      npar=npar1
      call ptf2par(ptf,npar,nfile,para)
      newpar=1

      idfold=0
      mflag=1
!$OMP PARALLEL COPYIN(/fitpar/)
!$OMP DO PRIVATE(yf) FIRSTPRIVATE(idfold)

      do idat=1,ndat

        idf1=idf(idat)
        if ((idf1.ne.idfold).and.(ipgi.ne.0)) newpar=1

        do ipar=1,npar
          par(ipar)=para(ipar,idf1)
        end do

C        if ((itrace.ge.4).or.
C     &     ((itrace.eq.2).and.(idf1.eq.1).and.(idfold.ne.idf1)).or.
C     &     ((itrace.ge.3).and.(idfold.ne.idf1))) then
C          do ipar=1,npar
C            write(*,'(e14.6,$)') par(ipar)
C          end do
C          if (itrace.ge.4) then
C            write(*,'(e14.6,$)') xd(idat)
C          else
C            write(*,*)
C          end if
C        end if
C
        yf=convfun(xd(idat))

C        if (itrace.ge.4) then
C          write(*,'(2e14.6)') xd(idat),yf
C        end if
C

        if ((itrace.ge.4).or.
     &     ((itrace.eq.2).and.(idf1.eq.1).and.(idfold.ne.idf1)).or.
     &     ((itrace.ge.3).and.(idfold.ne.idf1))) then
          do ipar=1,npar
            write(*,'(e14.6,$)') par(ipar)
          end do
          if (itrace.ge.4) then
            write(*,'(2e14.6)') xd(idat),yf
          else
            write(*,*)
          end if
        end if

        fct(idat)=yf

        if (iweight.ne.0) then
          f(idat)=(yd(idat)-yf)*swd(idat)
        else
          f(idat)=yd(idat)-yf
        end if

        idfold=idf1

      end do

!$OMP END DO
!$OMP END PARALLEL

        if (mflag.gt.3) then
          write(*,'("Convolution warning! Status: ",i1)') mflag
        else
          if ((itrace.ge.2).and.(mflag.gt.1)) then
            write(*,'("Convolution integral status: ",i1)') mflag
          end if
        end if

      if (itrace.ge.1) then

        ssq=0.0
        do idat=1,ndat
          ssq=ssq+f(idat)**2
        end do
        write(*,'(a,i4,a,e12.5)') 'Step:',nfun,'  Sum of squares:',ssq

      end if

      if (imoni>0) then
        open(57,file='unifit_moni.txt',status='unknown')
        idfold=0
        do idat=1,ndat
          idf1=idf(idat)
          if (idf1.ne.idfold) then
            write(57,*)
            idfold=idf1
          end if
          write(57,'(i6,4e14.6)')
     &      idf1,xd(idat),yd(idat),ed(idat),fct(idat)
        end do
        close(57)
      end if

      return
      end


      real*8 function convfun(x)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension par(mpar),para(mpar,mfile),ptf(mptf)
      dimension xd(mdatt),yd(mdatt),ed(mdatt),swd(mdatt),idf(mdatt)
      dimension xr(mres,mfile),yr(mres,mfile),idr(mfile),nres(mfile)
      dimension iconv(mfile),inono(mfile)
      dimension d(mdel),w(mdel)
      dimension f(mdatt),fct(mdatt)
      external fitfun,cfun

C     this common corresponds to FITFUN and CFUN:
      common/fitpar/ npar,idf1,newpar,idel,d,w,aconv,bconv,par
!$OMP THREADPRIVATE(/fitpar/)

C     these commons correspond to the main program:
      common/dat/ xd,yd,ed,swd,idf,iweight,npar1,nfile,nfun,
     &  itrace,ipgi,imoni
      common/conv/ xr,yr,nres,iconv,idr,inono,mflag

C     this common corresponds to KOVA:
      common/ff/ fct

C      write(*,*) (par(ipar),ipar=1,npar),">"
      if (iconv(idf1).eq.0) yf=fitfun(x)

      if (iconv(idf1).ne.0) then
        idr1=idr(idf1)
        if (inono(idr1).eq.0) then
          sr=0.5*(xr(2,idr1)-xr(1,idr1))*yr(1,idr1)
          do j=2,nres(idr1)-1
            sr=sr+0.5*(xr(j+1,idr1)-xr(j-1,idr1))*yr(j,idr1)
          end do
          sr=sr+0.5*(xr(nres(idr1),idr1)-xr(nres(idr1)-1,idr1))
     &       *yr(nres(idr1),idr1)
          rnorm=1.0/sr
        else
          rnorm=1.0
        end if
        if ((iconv(idf1).eq.2).or.(iconv(idf1).eq.4)) then
          do jdel=1,mdel
            w(jdel)=0.0
          end do
          idel=1
          dum=fitfun(xd(1))
        end if
      end if
      idel=0

      if (iconv(idf1).eq.1) then
        sum=0.5*(xr(2,idr1)-xr(1,idr1))*yr(1,idr1)
     &    *fitfun(x-xr(1,idr1))
        do j=2,nres(idr1)-1
          sum=sum+0.5*(xr(j+1,idr1)-xr(j-1,idr1))*yr(j,idr1)
     &      *fitfun(x-xr(j,idr1))
        end do
        sum=sum+0.5*(xr(nres(idr1),idr1)-xr(nres(idr1)-1,idr1))
     &    *yr(nres(idr1),idr1)*fitfun(x-xr(nres(idr1),idr1))
        yf=sum*rnorm
      end if

      if (iconv(idf1).eq.2) then
C     regular part:
        sum=0.0d0
        do j=1,nres(idr1)-1
          dx1=xr(j+1,idr1)-x
          dx2=x-xr(j,idr1)
          aconv=(dx1*yr(j,idr1)+dx2*yr(j+1,idr1))/(dx1+dx2)
          bconv=(yr(j,idr1)-yr(j+1,idr1))/(dx1+dx2)
          call qa05ad(val,cfun,-dx1,dx2,0.0d0,1.0d-7,1,err,iflag)
          if (iflag.gt.mflag) mflag=iflag
          sum=sum+val
        end do
C     delta functions part:
        do jdel=1,mdel
          do j=1,nres(idr1)-1
            dxd=x-d(jdel)
            if ((dxd.gt.xr(j,idr1)).and.
     &          (dxd.le.xr(j+1,idr1))) then
              dx1=xr(j+1,idr1)-dxd
              dx2=dxd-xr(j,idr1)
              sum=sum+w(jdel)*(dx1*yr(j,idr1)+dx2*yr(j+1,idr1))
     &                /(dx1+dx2)
            end if
          end do
        end do
        yf=sum*rnorm
      end if

C     Old numeric: treat resolution as histogram
      if (iconv(idf1).eq.4) then
C     regular part:
        call qa05ad(val,fitfun,
     &    x-0.5*(xr(1,idr1)+xr(2,idr1)),
     &    x-xr(1,idr1),
     &    0.0d0,1.0d-7,1,err,iflag)
        if (iflag.gt.mflag) mflag=iflag
        sum=yr(1,idr1)*val
        do j=2,nres(idr1)-1
          call qa05ad(val,fitfun,
     &      x-0.5*(xr(j,idr1)+xr(j+1,idr1)),
     &      x-0.5*(xr(j-1,idr1)+xr(j,idr1)),
     &      0.0d0,1.0d-7,1,err,iflag)
          if (iflag.gt.mflag) mflag=iflag
          sum=sum+yr(j,idr1)*val
        end do
        call qa05ad(val,fitfun,
     &    x-xr(nres(idr1),idr1),
     &    x-0.5*(xr(nres(idr1)-1,idr1)+xr(nres(idr1),idr1)),
     &    0.0d0,1.0d-7,1,err,iflag)
        if (iflag.gt.mflag) mflag=iflag
        sum=sum+yr(nres(idr1),idr1)*val
C     delta functions part:
        do jdel=1,mdel
          if (w(jdel).ne.0.0) then
            dxd=x-d(jdel)
            if ((dxd.gt.xr(1,idr1)).and.
     &          (dxd.le.0.5*(xr(1,idr1)+xr(2,idr1)))) then
              sum=sum+w(jdel)*yr(1,idr1)
            end if
            do j=2,nres(idr1)-1
              if ((dxd.gt.0.5*(xr(j-1,idr1)+xr(j,idr1))).and.
     &            (dxd.le.0.5*(xr(j,idr1)+xr(j+1,idr1)))) then
                sum=sum+w(jdel)*yr(j,idr1)
              end if
            end do
            if ((dxd.gt.0.5*(xr(nres(idr1)-1,idr1)
     &          +xr(nres(idr1),idr1)))
     &          .and.(dxd.lt.xr(nres(idr1),idr1))) then
              sum=sum+w(jdel)*yr(nres(idr1),idr1)
            end if
          end if
        end do
        yf=sum*rnorm
      end if

      if (iconv(idf1).eq.3) then
        sum=yr(1,idr1)*(fitfun(x-xr(1,idr1))
     &       -fitfun(x-0.5*(xr(1,idr1)+xr(2,idr1))))
        do j=2,nres(idr1)-1
          sum=sum+yr(j,idr1)
     &      *(fitfun(x-0.5*(xr(j-1,idr1)+xr(j,idr1)))
     &         -fitfun(x-0.5*(xr(j,idr1)+xr(j+1,idr1))))
        end do
        sum=sum+yr(nres(idr1),idr1)*(fitfun(x
     &    -0.5*(xr(nres(idr1)-1,idr1)+xr(nres(idr1),idr1)))
     &       -fitfun(x-xr(nres(idr1),idr1)))
        yf=sum*rnorm
      end if

      convfun=yf
      newpar=0

      return
      end


      real*8 function cfun(tt)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension par(mpar),d(mdel),w(mdel)

      external fitfun

      common/fitpar/ npar,idf1,newpar,idel,d,w,aconv,bconv,par
!$OMP THREADPRIVATE(/fitpar/)

      cfun=(aconv+bconv*tt)*fitfun(tt)

      return
      end


      character*50 function parse(line,nparq)

C     returns the nparq-th component of line (separated by blank space)
C     converted to lowercase

      character*1024 line
      character*50 param
      character och,TAB
C     parameter(TAB=char(9))

      TAB=char(9)
      ich0=1
      npar=1

C     skip blanks and tabs

 10   do ich=ich0,1024
        if ((line(ich:ich).ne.' ').and.(line(ich:ich).ne.TAB)) goto 20
      end do
      param=' '
      goto 99
 20   ich0=ich

C     read parameter

      jch=1
      param=' '
      do ich=ich0,1024
        if ((line(ich:ich).ne.' ').and.(line(ich:ich).ne.TAB)) then
          och=line(ich:ich)
          if (jch.le.50) then
            if (('A'.le.och).and.(och.le.'Z')) then
              param(jch:jch)=char(ichar(och)+32)
            else
              param(jch:jch)=och
            end if
          end if
          jch=jch+1
          if (jch.gt.50) goto 99
        else
          goto 30
        end if
      end do
      goto 99
 30   if (npar.eq.nparq) goto 99
      npar=npar+1
      ich0=ich
      goto 10

 99   parse=param
C     write(*,*) nparq,'|',param,'|'
      return
      end


      character*50 function parse1(line,nparq)

C     returns the nparq-th component of line (separated by blank space)
C     NOT converted to lowercase

      character*1024 line
      character*50 param
      character och,TAB
C     parameter(TAB=char(9))

      TAB=char(9)
      ich0=1
      npar=1

C     skip blanks and tabs

 10   do ich=ich0,1024
        if ((line(ich:ich).ne.' ').and.(line(ich:ich).ne.TAB)) goto 20
      end do
      param=' '
      goto 99
 20   ich0=ich

C     read parameter

      jch=1
      param=' '
      do ich=ich0,1024
        if ((line(ich:ich).ne.' ').and.(line(ich:ich).ne.TAB)) then
          param(jch:jch)=line(ich:ich)
          jch=jch+1
          if (jch.gt.50) goto 99
        else
          goto 30
        end if
      end do
      goto 99
 30   if (npar.eq.nparq) goto 99
      npar=npar+1
      ich0=ich
      goto 10

 99   parse1=param
C     write(*,*) nparq,'|',param,'|'
      return
      end


      integer function length(string)

      character*(*) string

      do 15,i=len(string),1,-1
        if (string(i:i).ne.' ') goto 20
 15   continue

 20   length=i

      return
      end


      real*8 function val(str)

      character*50 str

C     write(*,*) '!',str,'!'
      if (str.eq.'') goto 10
      read(str,*,err=10) val
      goto 20

 10   val=0.0

 20   return
      end


      real*8 function val1(str)

      include "unifit_par.f"

      character*50 str

C     write(*,*) '!',str,'!'
      if (str.eq.'') goto 10
      read(str,*,err=10) val1
      goto 20

 10   val1=amagic

 20   return
      end


      function indpar(str)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      character partyp(mpar)
      character*50 str,parnam(mpar)
      dimension parvar(mpar,mfile),parini(mpar,mfile),igroup(mpar,mfile)
      dimension isca(mpar,mfile),asca(6,mpar,mfile)

C     This common corresponds to the main program:
      common/pardef/ asca,isca,parini,parvar,parnam,igroup,partyp

C     write(*,*) str
      do ipar=1,mpar
        if (str.eq.parnam(ipar)) then
          indpar=ipar
          goto 99
        end if
      end do
      indpar=0

 99   continue
C     write(*,*) indpar

      return
      end



      subroutine rdpar(line,iarg,parin1,parva1,parmi1,parma1)

C     This subroutine reads a parameter value block (initial value,
C     [variation], [minimum], [maximum]) from the comman line starting
C     at argument iarg.

      implicit real*8 (a-h,o-z)

      character*1024 line
      character*50 arg,parse
      external val,parse

      parin1=val(parse(line,iarg))
      arg=parse(line,iarg+1)
      if (arg(1:3).eq.'min') then
        parmi1=val(parse(line,iarg+2))
        arg=parse(line,iarg+3)
        if (arg(1:3).eq.'max') then
          parma1=val(parse(line,iarg+4))
        end if
      else
        if (arg(1:3).eq.'max') then
          parma1=val(parse(line,iarg+2))
          arg=parse(line,iarg+3)
          if (arg(1:3).eq.'min') then
            parmi1=val(parse(line,iarg+4))
          end if
        else
          parva2=val(arg)
          if (parva2.ne.0.0) parva1=parva2
          arg=parse(line,iarg+2)
          if (arg(1:3).eq.'min') then
            parmi1=val(parse(line,iarg+3))
            arg=parse(line,iarg+4)
            if (arg(1:3).eq.'max') then
              parma1=val(parse(line,iarg+5))
            end if
          else
            if (arg(1:3).eq.'max') then
              parma1=val(parse(line,iarg+3))
              arg=parse(line,iarg+4)
              if (arg(1:3).eq.'min') then
                parmi1=val(parse(line,iarg+5))
              end if
            else
              if (arg(1:1).ne.' ') then
                  parmi1=val(arg)
                  arg=parse(line,iarg+3)
                    if (arg(1:1).ne.' ') then
                      parma1=val(arg)
                    end if
                end if
            end if
          end if
      end if
      end if

      return
      end


      subroutine ptf2par(ptf,npar,nfile,para)

C     This subroutine calculates and rearranges parameters-to-fit
C     (internal parameters) to an array of physical parameters.

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      character partyp(mpar)
      character*50 parnam(mpar)
      dimension parini(mpar,mfile),parvar(mpar,mfile),para(mpar,mfile)
      dimension isca(mpar,mfile),asca(6,mpar,mfile)
      dimension igroup(mpar,mfile)
      dimension ptf(mptf)

C     This common corresponds to the main program:
      common/pardef/ asca,isca,parini,parvar,parnam,igroup,partyp

      iptf=0
      do ipar=1,npar

        if (partyp(ipar).eq.'f') then
          iptf=iptf+1
          do ifile=1,nfile
            para(ipar,ifile)=pscale(ptf(iptf),isca(ipar,1),
     &                              asca(1,ipar,1))
          end do
        end if

        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
            iptf=iptf+1
            para(ipar,ifile)=pscale(ptf(iptf),isca(ipar,ifile),
     &                              asca(1,ipar,ifile))
          end do
        end if

        if (partyp(ipar).eq.'g') then
          maxg=0
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
            if (ig.gt.0) then
              if (ig.gt.maxg) maxg=ig
              para(ipar,ifile)=pscale(ptf(iptf+ig),isca(ipar,ig),
     &                              asca(1,ipar,ig))
            else
              para(ipar,ifile)=parini(ipar,mfile+ig+1)
            end if
C           if (ig.gt.maxg) maxg=ig
C           para(ipar,ifile)=ptf(iptf+ig)*parvar(ipar,ig)
C    &                       +parini(ipar,ig)
CC          write(*,*) iptf,ig,ptf(iptf+ig),
CC    &                 ipar,ifile,para(ipar,ifile)
          end do
C         write(*,*) maxg
          iptf=iptf+maxg
        end if

        if (partyp(ipar).eq.'c') then
          do ifile=1,nfile
            para(ipar,ifile)=parini(ipar,1)
          end do
        end if

        if (partyp(ipar).eq.'p') then
          do ifile=1,nfile
            para(ipar,ifile)=parini(ipar,ifile)
          end do
        end if

      end do

      return
      end


      subroutine dptf2dp(ptf,dptf,npar,nfile,dpara)

C     This subroutine calculates and rearranges the errors
C     calculated for the parameters-to-fit
C     to the errors of the physical parameters.

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      character partyp(mpar)
      character*50 parnam(mpar)
      dimension parini(mpar,mfile),parvar(mpar,mfile),dpara(mpar,mfile)
      dimension isca(mpar,mfile),asca(6,mpar,mfile)
      dimension igroup(mpar,mfile)
      dimension ptf(mptf),dptf(mptf)

C     This common corresponds to the main program:
      common/pardef/ asca,isca,parini,parvar,parnam,igroup,partyp

      iptf=0
      do ipar=1,npar

        if (partyp(ipar).eq.'f') then
          iptf=iptf+1
          do ifile=1,nfile
            dpara(ipar,ifile)=dpscale(ptf(iptf),dptf(iptf),
     &                                isca(ipar,1),asca(1,ipar,1))
          end do
        end if

        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
            iptf=iptf+1
            dpara(ipar,ifile)=dpscale(ptf(iptf),dptf(iptf),
     &                          isca(ipar,ifile),asca(1,ipar,ifile))
          end do
        end if

        if (partyp(ipar).eq.'g') then
          maxg=0
          do ifile=1,nfile
            ig=igroup(ipar,ifile)
            if (ig.gt.0) then
              if (ig.gt.maxg) maxg=ig
              dpara(ipar,ifile)=dpscale(ptf(iptf+ig),dptf(iptf+ig),
     &                                isca(ipar,ig),asca(1,ipar,ig))
            end if
          end do
          iptf=iptf+maxg
        end if

      end do

      return
      end


      subroutine cgen(pini,pvar,pmin,pmax,isca,asca)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension asca(6)
      
      if (pmin.eq.amagic) then

        if (pmax.eq.amagic) then

          isca=0
          asca(1)=pini
          asca(2)=pvar

        else

          isca=2
          asca(1)=pmax
          asca(2)=pmax-pini
          asca(3)=min(pvar/(pmax-pini),1.0d0)

        end if

      else

        if (pmax.eq.amagic) then

          isca=1
          asca(1)=pmin
          asca(2)=pini-pmin
          asca(3)=min(pvar/(pini-pmin),1.0d0)

        else

          isca=3
          asca(1)=pmin
          asca(2)=pini-pmin
          asca(3)=min(pvar/(pini-pmin),1.0d0)
          asca(4)=pmax
          asca(5)=pmax-pini
          asca(6)=min(pvar/(pmax-pini),1.0d0)

        end if

      end if

C     write(*,*) pini,pvar,pmin,pmax,isca,asca

      return
      end


      real*8 function pscale(ptf,isca,a)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension a(6)

      if (isca.eq.0) then
        pscale=a(2)*ptf+a(1)
        goto 499
      end if

      if (isca.eq.1) then
        pscale=a(2)*exp(a(3)*ptf)+a(1)
        goto 499
      end if

      if (isca.eq.2) then
        pscale=a(1)-a(2)*exp(-a(3)*ptf)
        goto 499
      end if

      if (isca.eq.3) then
        if (ptf.ge.0.0) then
        pscale=a(4)-a(5)*exp(-a(6)*ptf)
      else
        pscale=a(2)*exp(a(3)*ptf)+a(1)
      end if
      end if

 499  return
      end


      real*8 function dpscale(ptf,dptf,isca,a)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension a(6)

      if (isca.eq.0) then
        dpscale=a(2)*dptf
        goto 498
      end if

      if (isca.eq.1) then
        dpscale=a(2)*a(3)*exp(a(3)*ptf)*dptf
        goto 498
      end if

      if (isca.eq.2) then
        dpscale=a(2)*a(3)*exp(-a(3)*ptf)*dptf
        goto 498
      end if

      if (isca.eq.3) then
        if (ptf.ge.0.0) then
        dpscale=a(5)*a(6)*exp(-a(6)*ptf)*dptf
      else
        dpscale=a(2)*a(3)*exp(a(3)*ptf)*dptf
      end if
      end if

 498  return
      end


      subroutine wrfl(iline,iofile,npar,nfile,parx)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      character partyp(mpar)
      character*50 parnam(mpar)
      dimension parini(mpar,mfile),parvar(mpar,mfile),dpara(mpar,mfile)
      dimension parx(mpar,mfile)
      dimension isca(mpar,mfile),asca(6,mpar,mfile)
      dimension igroup(mpar,mfile)
      dimension ptf(mptf),dptf(mptf)

C     This common corresponds to the main program:
      common/pardef/ asca,isca,parini,parvar,parnam,igroup,partyp

      if ((iline.eq.1).or.(iline.eq.2)) then
        write(iofile,'("#"$)')
      else
        write(iofile,'(" "$)')
      end if

      do ipar=1,npar

        if (partyp(ipar).eq.'f') then
          if (iline.eq.1) write(iofile,'(x,a11$)') parnam(ipar)
          if (iline.eq.2) write(iofile,'(a12$)') "           *"
          if (iline.eq.3) write(iofile,'(e12.4$)') parx(ipar,1)
        end if

        if (partyp(ipar).eq.'i') then
          do ifile=1,nfile
          if (iline.eq.1) write(iofile,'(x,a11$)') parnam(ipar)
          if (iline.eq.2) write(iofile,'(i12$)') ifile
          if (iline.eq.3) write(iofile,'(e12.4$)') parx(ipar,ifile)
          end do
        end if

        if (partyp(ipar).eq.'g') then
          maxg=0
          do ifile=1,nfile
            if (igroup(ipar,ifile).gt.maxg) then
              if (iline.eq.1) write(iofile,'(x,a11$)') parnam(ipar)
              if (iline.eq.2) write(iofile,'(i12$)') ifile
              if (iline.eq.3) write(iofile,'(e12.4$)') parx(ipar,ifile)
              maxg=igroup(ipar,ifile)
            end if
          end do
        end if

      end do

      write(iofile,*)

      return
      end


      subroutine  mcfit(ndat,nptf,f,ptf,maxfun)

      implicit real*8 (a-h,o-z)
      include "unifit_par.f"
      dimension ptf(mptf),ptf0(mptf),f(mdatt)

      do istep=0,maxfun
        do ip=1,nptf
          ptf0(ip)=ptf(ip)
          if (istep.gt.0) ptf(ip)=ptf(ip)+grand(0)
C         write(*,*) ptf(ip),ptf0(ip)
        end do
        call calfun(ndat,nptf,f,ptf)
        ssq=0.0
        do idat=1,ndat
          ssq=ssq+f(idat)**2
        end do
        if ((istep.gt.0).and.(ssq.ge.ssqmin)) then
          do ip=1,nptf
            ptf(ip)=ptf0(ip)
C           write(*,*) ptf(ip),ptf0(ip)
          end do
        else
          ssqmin=ssq
          write(*,'(a,i4,a,e13.5)')
     &      'Successful MC step:',istep,' Sum of squares: ',ssq
        end if
      end do
      call calfun(ndat,nptf,f,ptf)

      return
      end


      real*8 function grand(iseed)

      implicit none
      real*8 x1,x2,w
      integer iseed,istate,magic
      parameter(magic=1037425391)
      common/memory/ x1,x2,w,istate

      if (istate.ne.magic) then
 10     x1=2.0*rand(iseed)-1.0
        x2=2.0*rand(0)-1.0
        w=x1**2+x2**2
        if (w.gt.1.0) goto 10
        w=sqrt((-2.0*log(w))/w)
        grand=x1*w
        istate=magic
      else
        grand=x2*w
        istate=0
      end if

      return
      end
